package appcontroller;

/**
 * Описывает элемент таблицы для отображения информации об уровне нейронной сети
 */
public class NeuralLayerTableItem {
    private int mNumber;//Номер уровня
    private int mNeuroneCount;//Число нейронов

    /**
     * Создает объект уровня сети с 0 уровнем и числом нейронов равным 0
     */
    public NeuralLayerTableItem() {
    }

    /**
     * Создает обьект уровня сети
     *
     * @param number       Номер уровня
     * @param neuroneCount Число нейронов
     */
    public NeuralLayerTableItem(int number, int neuroneCount) {
        mNumber = number;
        mNeuroneCount = neuroneCount;
    }

    /**
     * Возвращает номер уровня
     */
    public int getNumber() {
        return mNumber;
    }

    /**
     * Задает номер уровня
     *
     * @param number Номер уровня
     */
    public void setNumber(int number) {
        mNumber = number;
    }

    /**
     * Возвращает число нейронов на уровне
     */
    public int getNeuroneCount() {
        return mNeuroneCount;
    }

    /**
     * Задает число нейронов на уровне
     *
     * @param neuroneCount Число нейронов
     */
    public void setNeuroneCount(int neuroneCount) {
        mNeuroneCount = neuroneCount;
    }
}
