package appcontroller;

import weatherdata.ComparablePredictedTemperature;
import weatherdata.DataOperations;
import weatherdata.DateOperations;

import javax.swing.table.AbstractTableModel;
import java.util.List;

/**
 * Описывает модель таблицы для отображения данных сравнительного анализа
 */
public class ComparableAnalysisTableMode extends AbstractTableModel {
    private List<ComparablePredictedTemperature> mPredictions;//Прогнозируемые наблюдения температуры

    /**
     * Создает модель таблицы
     *
     * @param predictions Пронозируемые наблюдения температуры
     */
    public ComparableAnalysisTableMode(List<ComparablePredictedTemperature> predictions) {
        super();
        mPredictions = predictions;
    }

    @Override
    public int getRowCount() {
        return mPredictions.size();
    }

    @Override
    public int getColumnCount() {
        return 7;
    }

    @Override
    public String getColumnName(int column) {
        String result = "";
        switch (column) {
            case 0: {
                result = "№";
                break;
            }
            case 1: {
                result = "Дата";
                break;
            }
            case 2: {
                result = "Наблюдение";
                break;
            }
            case 3: {
                result = "Нейросеть";
                break;
            }
            case 4: {
                result = "Аппроксимация";
                break;
            }
            case 5: {
                result = "Ошибка нейросети";
                break;
            }
            case 6: {
                result = "Ошибка аппроксимации";
                break;
            }
        }
        return result;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0: {
                return mPredictions.get(rowIndex).getNumber();
            }
            case 1: {
                return DateOperations.stringRepresentation(mPredictions.get(rowIndex).getObservationDate());
            }
            case 2: {
                return DataOperations.round(mPredictions.get(rowIndex).getObservableTemperature(), 1);
            }
            case 3: {
                return DataOperations.round(mPredictions.get(rowIndex).getNeuralNetworkPredictedTemperature(), 1);
            }
            case 4: {
                return DataOperations.round(mPredictions.get(rowIndex).getSplinePredictedTemperature(), 1);
            }
            case 5: {
                return DataOperations.round(mPredictions.get(rowIndex).getPredictionError().getNeuralNetworkPredictionError(), 1);
            }
            case 6: {
                return DataOperations.round(mPredictions.get(rowIndex).getPredictionError().getSplinePredictionError(), 1);
            }
            default: {
                return "";
            }
        }
    }
}
