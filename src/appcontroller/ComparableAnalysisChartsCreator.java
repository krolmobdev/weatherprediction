package appcontroller;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import weatherdata.ComparablePredictedTemperature;
import weatherdata.DataOperations;
import weatherdata.PredictionError;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Предназначен для создания 2 диаграмм с результатами сравнительного анализа:
 * 1 диаграмма содержит 3 графика:
 * температурные наблюдения, прогноз температуры нейронной сетью, прогноз температуры аппроксимацией
 * 2 диграмма содержит сигналы ошибки пронозирования методом нейронной сети и аппроксимации
 */
public class ComparableAnalysisChartsCreator {
    private List<ComparablePredictedTemperature> mPredictions;//пронозируемые наблюдения температуры

    /**
     * Предназначен для создания пустой диаграммы
     */
    public ComparableAnalysisChartsCreator() {
        this(new ArrayList<ComparablePredictedTemperature>());
    }

    /**
     * Создает диаграмму с прогнозируемыми наблюдениями температуры
     *
     * @param predictions Прогнозируемые наблюдения температуры
     */
    public ComparableAnalysisChartsCreator(List<ComparablePredictedTemperature> predictions) {
        mPredictions = predictions;
    }

    /**
     * Возвращает данные прогноза, отображаемые на диаграмме
     */
    public List<ComparablePredictedTemperature> getPredictions() {
        return mPredictions;
    }

    /**
     * Задает данные прогноза, которые будут отображаться на диграмме
     *
     * @param predictions Данные прогноза
     */
    public void setPredictions(List<ComparablePredictedTemperature> predictions) {
        mPredictions = predictions;
    }

    /**
     * Создает пустую панель для отображения диаграммы
     *
     * @param bounds Параметры размещения панели
     */
    public ChartPanel createEmptyChartPanel(Rectangle bounds) {
        ChartPanel chartPanel = new ChartPanel((JFreeChart) null);
        bounds.setSize(bounds.width, bounds.height - 30);
        chartPanel.setBounds(bounds);

        return chartPanel;
    }

    /**
     * Возвращает диаграмму, которая отображает результаты сравнительного анализа методов
     * пронозирования температуры: нейронной сетью и аппроксимацией
     *
     * @return Диаграмма результатов сравнительного анализа: содержит 3 графика: наблюдения, прогноз нейронной сети,
     * прогноз аппроксимацией
     */
    public JFreeChart createCompatibleAnalysisChart() {
        XYSeriesCollection seriesCollection = new XYSeriesCollection();

        XYSeries observationsXYSeries = new XYSeries("Наблюдения");
        XYSeries neuralNetworkPredictionsSeries = new XYSeries("Прогноз нейросети");
        XYSeries splinePredictionsSeries = new XYSeries("Прогноз аппроксимацией");

        for (ComparablePredictedTemperature prediction : mPredictions) {
            observationsXYSeries.add(prediction.getNumber(), prediction.getObservableTemperature());
            neuralNetworkPredictionsSeries.add(prediction.getNumber(), DataOperations.round(prediction.getNeuralNetworkPredictedTemperature(), 1));
            splinePredictionsSeries.add(prediction.getNumber(), DataOperations.round(prediction.getSplinePredictedTemperature(), 1));
        }

        seriesCollection.addSeries(observationsXYSeries);
        seriesCollection.addSeries(neuralNetworkPredictionsSeries);
        seriesCollection.addSeries(splinePredictionsSeries);

        JFreeChart chart = ChartFactory.createXYLineChart("Сравнительный анализ методов прогнозирования температуры",
                "№ наблюдения", "температура",
                seriesCollection,
                PlotOrientation.VERTICAL,
                true, true, true);

        return chart;
    }

    /**
     * Возвращает диаграмму с графиками ошибки прогнозирования методами нейронной сети и аппоксимации
     *
     * @return Диаграмма результатов ошибки прогнозирования: содержит 2 графика: ошибка прогнозирования
     * нейронной сети, ошибка прогнозирования аппоксимацией
     */
    public JFreeChart createPredictionMethodsErrorChart() {
        XYSeriesCollection seriesCollection = new XYSeriesCollection();

        XYSeries neuralNetworkErrorSeries = new XYSeries("Ошибка прогноза нейросети");
        XYSeries splineErrorSeries = new XYSeries("Ошибка прогноза аппроксимацией");

        for (ComparablePredictedTemperature prediction : mPredictions) {
            PredictionError error = prediction.getPredictionError();
            neuralNetworkErrorSeries.add(prediction.getNumber(), DataOperations.round(error.getNeuralNetworkPredictionError(), 1));
            splineErrorSeries.add(prediction.getNumber(), DataOperations.round(error.getSplinePredictionError(), 1));
        }

        seriesCollection.addSeries(neuralNetworkErrorSeries);
        seriesCollection.addSeries(splineErrorSeries);

        JFreeChart chart = ChartFactory.createXYLineChart("Ошибка прогнозирования",
                "№ наблюдения", "значение ошибки",
                seriesCollection,
                PlotOrientation.VERTICAL,
                true, true, true);

        return chart;
    }
}
