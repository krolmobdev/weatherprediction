/*
 * Created by JFormDesigner on Thu Dec 28 19:27:47 MSK 2017
 */

package appcontroller;

import neuralnetwork.NeuralLayer;
import neuralnetwork.NeuralNetwork;
import neuralnetwork.NeuralNetworkRuntimeException;
import org.jfree.chart.ChartPanel;
import weatherdata.*;

import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author O O
 */
public class MainForm extends JFrame {
    public static final String NETWORK_FILENAME = "NeuralNetwork.ser";
    public static final String NORMALIZER_FILENAME = "TemperatureNormalizer.ser";
    public static final String NEXT_DAY_PREDICTION_DATA_FILENAME = "PredictionData.xls";
    public static final String COMPATIBLE_ANALYSIS_DATA_FILENAME = "CompatibleAnalysisData.xls";

    private TemperatureObservationsController mLeaningDataController;//Подготавливает данные для обучения нейронной сети
    private TemperatureObservationsController mNextDayPredictionDataController;//Подготавливает данные для прогнозирования на след. день
    private TemperatureObservationsController mCompatibleAnalysisDataController;//Подготавливает данные для сравнительного анализа методов
    private NeuralNetwork mNeuralNetwork;//Нейронная сеть для прогнозирования
    private TemperatureNormalizer mTemperatureNormalizer;//Нормализатор температуры

    private ChartPanel mComparableAnalysisPanel;//Панель для панелей с графиками сравнительного анализа методов прогнозирования
    private ChartPanel mPredictionMethodsErrorChartPanel;//Панель для графиков ошибок прогнозирования методов
    private ComparableAnalysisChartsCreator mComparableAnalysisChartsCreator;//Панель для графиков сравнительного анализа методов

    public MainForm() {
        initComponents();
        setVisible(true);

         // Задаем объект для нормализации температуры

        mTemperatureNormalizer = new TemperatureNormalizer(-40, 40);

        try {
            mTemperatureNormalizer = TemperatureNormalizer.loadNormalizer(NORMALIZER_FILENAME);

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        updateUIMinAndMaxTemperature();

        //Задаем объекты для загрузки данных для обучения и прогнозирования

        mLeaningDataController = new TemperatureObservationsController(mTemperatureNormalizer);

        mNextDayPredictionDataController = new TemperatureObservationsController(mTemperatureNormalizer);
        loadNextDayPredictionData();

        mCompatibleAnalysisDataController = new TemperatureObservationsController(mTemperatureNormalizer);
        loadComparableAnalysisData();

        //Задаем параметры нейронной сети по-умолчанию

        mNeuralNetwork = new NeuralNetwork(5);
        mNeuralNetwork.addLayer(17);
        mNeuralNetwork.addLayer(1);

        try {
            mNeuralNetwork=NeuralNetwork.loadNetwork(NETWORK_FILENAME);
            labelNeuralNetworkStatus.setText("Статус: загружена");

        } catch (IOException ex) {
            ex.printStackTrace();
        }

        updateUINeuralNetworkParameters();
        textAreaNeuralNetworkLeaningCharacteristics.setText(mNeuralNetwork.toString());

        //Задает модель таблицы сравнительного анализа методов прогнозирования

        tableComparativeAnalysis.setModel(new ComparableAnalysisTableMode(new ArrayList<ComparablePredictedTemperature>()));

        //Добавляем пустые панели сравнительного анализа методов

        mComparableAnalysisChartsCreator = new ComparableAnalysisChartsCreator();

        //Добавляет панель с пустой диаграммой сравнительного анализа методов прогнозирования
        mComparableAnalysisPanel = mComparableAnalysisChartsCreator.createEmptyChartPanel(panelChartComparableAnalysis.getBounds());
        mComparableAnalysisPanel.setChart(mComparableAnalysisChartsCreator.createCompatibleAnalysisChart());
        panelChartComparableAnalysis.add(mComparableAnalysisPanel);


        //Добавляет панель с пустой диаграммой ошибок методов пронозирования
        mPredictionMethodsErrorChartPanel = mComparableAnalysisChartsCreator.createEmptyChartPanel(panelChartComparableAnalysis.getBounds());
        mPredictionMethodsErrorChartPanel.setChart(mComparableAnalysisChartsCreator.createPredictionMethodsErrorChart());
        panelPredictionMethodsErrors.add(mPredictionMethodsErrorChartPanel);

        //Задает обработчикам ввода счетчиков возможность ввода только численных значений
        setSpinnersKeyListeners();
    }

    /**
     * Задает обработчикам ввода счетчиков возможность ввода только численных значений
     */
    private void setSpinnersKeyListeners() {
        JTextField textFieldSpinnerNNLayersCount = ((JSpinner.DefaultEditor) spinnerNNLayersCount.getEditor()).getTextField();
        textFieldSpinnerNNLayersCount.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                allowEnterNotNegativeIntegerValues(e);
            }
        });

        JTextField textFieldSpinnerNNImageDimension = ((JSpinner.DefaultEditor) spinnerNNImageDimension.getEditor()).getTextField();
        textFieldSpinnerNNImageDimension.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                allowEnterNotNegativeIntegerValues(e);
            }
        });

        JTextField textFieldSpinnerPredictingValuesCount = ((JSpinner.DefaultEditor) spinnerPredictingValuesCount.getEditor()).getTextField();
        textFieldSpinnerPredictingValuesCount.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                allowEnterNotNegativeIntegerValues(e);
            }
        });

        JTextField textFieldSpinnerPredictingValuesCount2=((JSpinner.DefaultEditor)spinnerPredictingValuesCount2.getEditor()).getTextField();
        textFieldSpinnerPredictingValuesCount2.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                allowEnterNotNegativeIntegerValues(e);
            }
        });
    }

    /**
     * Загружает данные для прогноза на следующий день
     */
    private void loadNextDayPredictionData() {
        try {
            mNextDayPredictionDataController.loadObservationsDataFromExcel(NEXT_DAY_PREDICTION_DATA_FILENAME);
            textAreaSampleForPrediction.setText(mNextDayPredictionDataController.toString());

        } catch (Exception ex) {
            showErrorMessageDialog("Ошибка загрузки данных:\n" + ex.getMessage());
        }
    }

    /**
     * Загружает данные для сравнительного анализа
     */
    private void loadComparableAnalysisData() {
        try {
            mCompatibleAnalysisDataController.loadObservationsDataFromExcel(COMPATIBLE_ANALYSIS_DATA_FILENAME);
            textAreaCompatibleAnalysisData.setText(mCompatibleAnalysisDataController.toString());

        } catch (IOException ex) {
            showErrorMessageDialog("Ошибка загрузки данных:\n" + ex.getMessage());
        }
    }

    /**
     * Отображает диалог с соообщением об ошибке
     *
     * @param message Сообщение ошибки
     */
    private void showErrorMessageDialog(String message) {
        JOptionPane.showMessageDialog(this, message, Program.NAME, JOptionPane.ERROR_MESSAGE);
    }

    /**
     * Отображает диалог с подтверждающим сообщением
     *
     * @param message Подтверждающее ссобщение
     */
    private void showInformMessageDialog(String message) {
        JOptionPane.showMessageDialog(this, message, Program.NAME, JOptionPane.INFORMATION_MESSAGE);
    }

    /**
     * Загружает данные для обучения нейронной сети
     *
     * @param e
     */
    private void buttonLoadLeaningSampleActionPerformed(ActionEvent e) {
        // TODO add your code here
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileFilter(new FileNameExtensionFilter("Excel file", "xls"));

        int ret = fileChooser.showDialog(null, "Выберите файл");

        String selectedFile = null;
        if (ret == JFileChooser.APPROVE_OPTION) {
            selectedFile = fileChooser.getSelectedFile().getAbsolutePath();
        }

        if (selectedFile != null) {
            try {
                mLeaningDataController.loadObservationsDataFromExcel(selectedFile);
                textAreaLeaningSample.setText(mLeaningDataController.toString());

            } catch (Exception ex) {
                showErrorMessageDialog("Ошибка загрузки данных:\n" + ex.getMessage());
            }

            labelNeuralNetworkStatus.setText("Статус: необучена");
        }
    }

    /**
     * Обучает нейронную сеть
     *
     * @param e
     */
    private void buttonLeanNeuralNetworkActionPerformed(ActionEvent e) {
        // TODO add your code here

        if (mLeaningDataController.isAllowableObservationsSizeForLeaningSample()) {

            Map<List<Double>, List<Double>> leaningSample
                    = mLeaningDataController.prepareLeaningSample(mNeuralNetwork.getImageDimension());

            labelNeuralNetworkStatus.setText("Статус: идет обучение...");

            setEnabledButtons(false);

            Thread leanerThread = new Thread(new Runnable() {
                @Override
                public void run() {

                    mNeuralNetwork.lean(leaningSample);

                    labelNeuralNetworkStatus.setText("Статус: обучена");
                    textAreaNeuralNetworkLeaningCharacteristics.setText(mNeuralNetwork.toString());

                    setEnabledButtons(true);
                }
            });
            leanerThread.start();

        } else {
            showErrorMessageDialog("Недопустимый размер обучающей выборки");
        }
    }

    /**
     * Устанавливает доступность кнопок
     *
     * @param isEnabled Доступность кнопок
     */
    private void setEnabledButtons(boolean isEnabled) {
        buttonLeanNeuralNetwork.setEnabled(isEnabled);
        buttonSaveNeuralNetwork.setEnabled(isEnabled);
        buttonPredict.setEnabled(isEnabled);
        buttonCreateNeuralNetworkWithSettedParameters.setEnabled(isEnabled);
        buttonGetNeuralNetworkParameters.setEnabled(isEnabled);
        buttonCompatibleAnalysis.setEnabled(isEnabled);
    }

    /**
     * Сериализует данные нейронной сети
     *
     * @param e
     */
    private void buttonSaveNeuralNetworkActionPerformed(ActionEvent e) {
        // TODO add your code here
        try {
            NeuralNetwork.saveNetwork(NETWORK_FILENAME, mNeuralNetwork);

            showInformMessageDialog("Сеть сохранена");

        } catch (IOException | NeuralNetworkRuntimeException ex) {
            showErrorMessageDialog("Ошибка сохранения сети:\n" + ex.getMessage());
        }
    }

    /**
     * Прогнозирует погоду на следующий день
     *
     * @param e
     */
    private void buttonNextDayPredictionActionPerformed(ActionEvent e) {
        // TODO add your code here
        TemperaturePredictor predictor = new TemperaturePredictor(mNextDayPredictionDataController, mNeuralNetwork);
        try {
            List<PredictedTemperature> nextDayTemperature = predictor.nextDayTemperature((Integer)spinnerPredictingValuesCount2.getValue());

            textAreaPredictedTemperature.setText(null);
            for (PredictedTemperature temperature:nextDayTemperature) {
                textAreaPredictedTemperature.append(temperature.toString()+"\n");
            }

        } catch (WeatherDataException ex) {
            showErrorMessageDialog(ex.getMessage());
        } catch (ClassCastException ex){
            showErrorMessageDialog("Задана недопустимая дальность прогноза");
        }
        catch (Exception ex) {
            showErrorMessageDialog("Ошибка прогнозирования:" + ex.getMessage());
        }
    }

    /**
     * Открывает excel-документ для изменения данных для прогноза на следующий день
     *
     * @param e
     */
    private void buttonChangeNexDayPredictionDataActionPerformed(ActionEvent e) {
        // TODO add your code here
        try {
            Desktop.getDesktop().open(new File(NEXT_DAY_PREDICTION_DATA_FILENAME));

        } catch (Exception ex) {
            showErrorMessageDialog("Ошибка открытия Excel-документа:\n" + ex.getMessage());
        }
    }

    /**
     * Загружает из excel-документа данные для прогноза на следующий день
     *
     * @param e
     */
    private void buttonUpdateNextDayPredictionDataActionPerformed(ActionEvent e) {
        // TODO add your code here
        loadNextDayPredictionData();
    }

    /**
     * Десериализует данные нейронной сети из файла
     *
     * @param e
     */
    private void buttonLoadNetworkActionPerformed(ActionEvent e) {
        // TODO add your code here
        try {
            mNeuralNetwork = NeuralNetwork.loadNetwork(NETWORK_FILENAME);

            labelNeuralNetworkStatus.setText("Статус: загружена");
            textAreaNeuralNetworkLeaningCharacteristics.setText(mNeuralNetwork.toString());

            updateUINeuralNetworkParameters();

        } catch (IOException | NeuralNetworkRuntimeException ex) {
            showErrorMessageDialog("Ошибка загрузки сети:\n" + ex.getMessage());
        }
    }

    /**
     * Обновляет графические компоненты, для отображения параметров нейронной сети
     */
    private void updateUINeuralNetworkParameters() {
        spinnerNNImageDimension.setValue(mNeuralNetwork.getImageDimension());
        spinnerNNLayersCount.setValue(mNeuralNetwork.getLayersCount());
        textFieldNNLeaningRate.setText(Double.toString(mNeuralNetwork.getLeaningRate()));
        textFieldNNMomentum.setText(Double.toString(mNeuralNetwork.getMomentum()));
        //Переводим из долей в % и выводим данные допустимого изменения ошибки обучения
        textFieldChangingLeaningError.setText(Double.toString(mNeuralNetwork.getChangingLeaningError() * 100));

        List<NeuralLayerTableItem> layers = new ArrayList<NeuralLayerTableItem>();

        int numLayer = 0;
        for (NeuralLayer layer : mNeuralNetwork.getLayers()) {
            layers.add(new NeuralLayerTableItem(numLayer, layer.getNeuroneCount()));
            numLayer++;
        }
        tableNNLayers.setModel(new NeuralLayersTableMode(layers));
    }

    /**
     * Выполняет сравнительный анализ прогноза погоды на заданное число дней по данным сравнительного анализа
     * методами нейронной сети и аппроксимацией
     *
     * @param e
     */
    private void buttonCompatibleAnalysisActionPerformed(ActionEvent e) {
        // TODO add your code here
        try {
            //Вычисляем данные сравнительного анализа

            TemperaturePredictor predictor = new TemperaturePredictor(mCompatibleAnalysisDataController, mNeuralNetwork);
            List<ComparablePredictedTemperature> predictions = predictor.compareTemperatureSequenceUsingPredictedValues((Integer) spinnerPredictingValuesCount.getValue());
            tableComparativeAnalysis.setModel(new ComparableAnalysisTableMode(predictions));

            //Обновляем диаграммы сравнительного анализа методов прогнозирования

            mComparableAnalysisChartsCreator.setPredictions(predictions);

            //Обновляем диаграмму сравнительного анализа методов прогнозирования
            mComparableAnalysisPanel.setChart(mComparableAnalysisChartsCreator.createCompatibleAnalysisChart());

            //Обновляем диаграмму ошибки методов прогнозирования
            mPredictionMethodsErrorChartPanel.setChart(mComparableAnalysisChartsCreator.createPredictionMethodsErrorChart());

            //Выводим данные об ошибках сравнительного анализа

            textAreaCompatibleAnalysisErrors.setText("Средние ошибки прогноза\n" +
                    TemperaturePredictor.averagePredictionError(predictions).toString());

            textAreaCompatibleAnalysisErrors.append("\n\nМаксимальные ошибки прогноза\n" +
                    TemperaturePredictor.maxPredictionError(predictions).toString());

        } catch (Exception ex) {
            showErrorMessageDialog("Ошибка сравнительного анализа:\n" + ex.getMessage());
        }
    }

    /**
     * Загружает из excel-документа данные нейронной сети
     *
     * @param e
     */
    private void buttonUpdateComparableAnalysisDataActionPerformed(ActionEvent e) {
        // TODO add your code here
        loadComparableAnalysisData();
    }

    /**
     * Открывает excel-документ для изменения данных сравнительного анализа
     *
     * @param e
     */
    private void buttonChangeComparableAnalysisDataActionPerformed(ActionEvent e) {
        // TODO add your code here
        try {
            Desktop.getDesktop().open(new File(COMPATIBLE_ANALYSIS_DATA_FILENAME));

        } catch (Exception ex) {
            showErrorMessageDialog("Ошибка открытия Excel-документа:\n" + ex.getMessage());
        }
    }

    /**
     * Позволяет вводить в графические компоненты только целочисленные неотрицательные значения
     *
     * @param e Событие нажатия клавиши клавиатуры
     */
    private void allowEnterNotNegativeIntegerValues(KeyEvent e) {
        char c = e.getKeyChar();
        if (((c < '0') || (c > '9')) && c != KeyEvent.VK_BACK_SPACE) {
            e.consume();  // игнорим введенные буквы и пробел
        }
    }

    /**
     * Позволяет вводить в графические компоненты только целочисленные значения
     *
     * @param e Событие нажатия клавиши клавиатуры
     */
    private void allowEnterIntegerValues(KeyEvent e) {
        char c = e.getKeyChar();
        if (((c < '0') || (c > '9')) && c != KeyEvent.VK_BACK_SPACE && c!='-') {
            e.consume();  // игнорим введенные буквы и пробел
        }
    }

    /**
     * Позволяет вводить в графические компоненты только вещественные неотрицательные значения
     *
     * @param e Событие нажатия клавиши клавиатуры
     */
    private void allowEnterNotNegativeFloatValues(KeyEvent e) {
        char c = e.getKeyChar();
        if (((c < '0') || (c > '9')) && c != KeyEvent.VK_BACK_SPACE && c != '.') {
            e.consume();  // игнорим введенные буквы и пробел
        }
    }

    private void textFieldNNMomentumKeyTyped(KeyEvent e) {
        // TODO add your code here
        allowEnterNotNegativeFloatValues(e);
    }

    private void textFieldNNLeaningRateKeyTyped(KeyEvent e) {
        // TODO add your code here
        allowEnterNotNegativeFloatValues(e);
    }

    private void textFieldChangingLeaningErrorKeyPressed(KeyEvent e) {
        // TODO add your code here
        allowEnterNotNegativeFloatValues(e);
    }

    /**
     * Создает новую нейронную сеть с заданными параметрами
     *
     * @param e
     */
    private void buttonCreateNeuralNetworkWithSettedParametersActionPerformed(ActionEvent e) {
        // TODO add your code here

        String inputNetworkErrors = checkNeuralNetworkParameters();

        if (inputNetworkErrors.equals("")) {
            //Если параметры сети не содержат ошибок, то создаем новую нейронную сеть
            // с заданными пользователем параметрами

            mNeuralNetwork = new NeuralNetwork((Integer) spinnerNNImageDimension.getValue());
            mNeuralNetwork.setLeaningRate(Double.parseDouble(textFieldNNLeaningRate.getText()));
            mNeuralNetwork.setMomentum(Double.parseDouble(textFieldNNMomentum.getText()));
            //Переводим допустимое изменение ошибки обучения из % в доли и задаем ее нейронной сети
            mNeuralNetwork.setChangingLeaningError(Double.parseDouble(textFieldChangingLeaningError.getText()) / 100);

            for (int i = 0; i < (Integer) spinnerNNLayersCount.getValue(); i++) {
                mNeuralNetwork.addLayer((Integer) tableNNLayers.getValueAt(i, 1));
            }

            labelNeuralNetworkStatus.setText("Статус: не обучена");
            textAreaNeuralNetworkLeaningCharacteristics.setText(mNeuralNetwork.toString());

            showInformMessageDialog("Сеть задана");
        } else {
            showErrorMessageDialog("Введены недопустимые параметры сети:" + inputNetworkErrors);
        }
    }

    /**
     * Проверяет правильность параметров сети, введенных в графические компоненты
     *
     * @return Если есть ошибки - строка с описанием имеющихся ошибок, иначе - пустая строка
     */
    private String checkNeuralNetworkParameters() {
        double momentum = 0.0;
        double leaningRate = 0.0;
        double changingRecognitionError = 0.0;
        int layersCount = 0;
        int imageDimension = 0;

        String error = "";
        try {
            imageDimension = (Integer) spinnerNNImageDimension.getValue();
            if (imageDimension == 0) error += "\nРазмерность образа равна нулю.";

        } catch (ClassCastException ex) {
            error += "\nЗадано недопустимое значение размерности образа.";
        }
        try {
            layersCount = (Integer) spinnerNNLayersCount.getValue();

            if (layersCount == 0) error += "\nЧисло уровней равно нулю.";

        } catch (ClassCastException ex) {
            error += "\nЗадано недопустимое значение числа уровней.";
        }

        try {
            momentum = Double.parseDouble(textFieldNNMomentum.getText());
            if (momentum < 0 || momentum > 1) throw new IllegalArgumentException();

        } catch (NumberFormatException ex) {
            error += "\nЗадано недопустимое значение коэффициента инерции.";
        }
        try {
            leaningRate = Double.parseDouble(textFieldNNLeaningRate.getText());
            if (leaningRate < 0 || leaningRate > 1) throw new IllegalArgumentException();

        } catch (IllegalArgumentException ex) {
            error += "\nЗадано недопустимое значение скорости обучения.";
        }
        try {
            changingRecognitionError = Double.parseDouble(textFieldChangingLeaningError.getText());
        } catch (NumberFormatException ex) {
            error += "\nЗадано недопустимое значение величины изменения ошибки пронозирования.";
        }
        for (int i = 0; i < layersCount; i++) {
            if ((Integer) tableNNLayers.getValueAt(i, 1) <= 0) {
                error += "\nНеположительное число нейронов на " + (i + 1) + " уровне";
            }
        }

        return error;
    }

    /**
     * Задает изменение числа уровней сети в таблице при изменении значения счетчика, отображающего число уровней
     *
     * @param e
     */
    private void spinnerNNLayersCountStateChanged(ChangeEvent e) {
        // TODO add your code here
        if (spinnerNNLayersCount.getValue() == "") {
            spinnerNNLayersCount.setValue(mNeuralNetwork.getLayersCount());
            return;
        }
        if (!(tableNNLayers.getModel() instanceof NeuralLayersTableMode)) {
            return;
        }

        NeuralLayersTableMode mode = (NeuralLayersTableMode) tableNNLayers.getModel();
        List<NeuralLayerTableItem> layers = mode.getNeuralLayers();

        int newCount = (Integer) spinnerNNLayersCount.getValue();
        int curCount = layers.size();

        if (newCount < curCount) {
            for (int i = 0; i < curCount - newCount; i++) {
                layers.remove(layers.size() - 1);
            }
            layers.get(layers.size() - 1).setNeuroneCount(1);

            ((NeuralLayersTableMode) tableNNLayers.getModel()).fireTableDataChanged();

        } else if (newCount > curCount) {
            for (int i = 0; i < newCount - curCount; i++) {
                layers.add(new NeuralLayerTableItem(layers.size(), 1));
            }

            ((NeuralLayersTableMode) tableNNLayers.getModel()).fireTableDataChanged();
        }
    }

    /**
     * Загружает значения графических компонентов, отображающих параметры нейронной сети из объекта,
     * содержащего данные сети
     *
     * @param e
     */
    private void buttonGetNeuralNetworkParametersActionPerformed(ActionEvent e) {
        // TODO add your code here
        updateUINeuralNetworkParameters();
    }

    private void textFieldMinTemperatureKeyTyped(KeyEvent e) {
        // TODO add your code here
        allowEnterIntegerValues(e);
    }

    private void textFieldMaxTemperatureKeyTyped(KeyEvent e) {
        // TODO add your code here
        allowEnterIntegerValues(e);
    }

    /**
     * Проверяет и задает новые значения минимальной и максимальной температуры для нормализации
     * @param e
     */
    private void buttonSetTemperatureNormalizationActionPerformed(ActionEvent e) {
        // TODO add your code here
        String error= checkMinAndMaxTemperature();

        if(error.equals("")){
            mTemperatureNormalizer.setMinTemperature(Double.parseDouble(textFieldMinTemperature.getText()));
            mTemperatureNormalizer.setMaxTemperature(Double.parseDouble(textFieldMaxTemperature.getText()));

            showInformMessageDialog("Значения температуры для нормализации заданы");

            try{
                TemperatureNormalizer.saveNormalizer(NORMALIZER_FILENAME,mTemperatureNormalizer);
            }catch (IOException ex){
                ex.printStackTrace();
            }
        }
    }

    /**
     * Проверяет допустимость введенных пользователем значений минимальной и максимальной температуры
     * @return Если введенные значения допустимые - возвращает пустую строку, иначе - строку с описанием ошибок
     */
    private String checkMinAndMaxTemperature() {

        String error = "";

        try {
            Double.parseDouble(textFieldMinTemperature.getText());

        } catch (NumberFormatException ex) {
            error += "\nЗадано недопустимое значение минимальной температуры.";
        }
        try {
           Double.parseDouble(textFieldMaxTemperature.getText());

        } catch (NumberFormatException ex) {
            error += "\nЗадано недопустимое значение максимальной температуры.";
        }

        return error;
    }

    /**
     * Обновляет значения графических компонентов, которые отображают значения температуры
     * @param e
     */
    private void buttonGetTemperatureNormalizationActionPerformed(ActionEvent e) {
        // TODO add your code here
        updateUIMinAndMaxTemperature();
    }

    /**
     * Обновляет значения графических компонентов, которые отображают значения температуры
     */
    private void updateUIMinAndMaxTemperature(){
        textFieldMaxTemperature.setText(Integer.toString((int) mTemperatureNormalizer.getMaxTemperature()));
        textFieldMinTemperature.setText(Integer.toString((int) mTemperatureNormalizer.getMinTemperature()));
    }


    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Oleg K
        tabbedPaneMain = new JTabbedPane();
        panelNeuralNetwork = new JPanel();
        scrollPaneLeaningSample = new JScrollPane();
        textAreaLeaningSample = new JTextArea();
        labelLeaningSample = new JLabel();
        labelNeuralNetworkStatus = new JLabel();
        buttonSaveNeuralNetwork = new JButton();
        scrollPaneNetworkData = new JScrollPane();
        textAreaNeuralNetworkLeaningCharacteristics = new JTextArea();
        labelNeuralNetworkParameters = new JLabel();
        buttonLoadLeaningSample = new JButton();
        panelNeuralNetworkParameters = new JPanel();
        scrollPane2 = new JScrollPane();
        tableNNLayers = new JTable();
        spinnerNNImageDimension = new JSpinner();
        labelNNImageDimension = new JLabel();
        spinnerNNLayersCount = new JSpinner();
        labelNNLayersCount = new JLabel();
        labelNNLayers = new JLabel();
        labelNNMomentumCoeff = new JLabel();
        textFieldNNMomentum = new JTextField();
        labelNNLeaningRate = new JLabel();
        textFieldNNLeaningRate = new JTextField();
        labelNNChangingRecognitionError = new JLabel();
        textFieldChangingLeaningError = new JTextField();
        buttonGetNeuralNetworkParameters = new JButton();
        labelNeuralNetworkLeaningCharacteristics = new JLabel();
        buttonLeanNeuralNetwork = new JButton();
        buttonCreateNeuralNetworkWithSettedParameters = new JButton();
        panelTemperatureNormalization = new JPanel();
        labelMinTemperature = new JLabel();
        labelMaxTemperature = new JLabel();
        textFieldMinTemperature = new JTextField();
        textFieldMaxTemperature = new JTextField();
        labelTemperatureNormalization = new JLabel();
        buttonSetMinAndMaxTemperature = new JButton();
        buttonGetMinAndMaxTemperature = new JButton();
        panelNextDayPrediction = new JPanel();
        labelNextDayPredictionData = new JLabel();
        scrollPaneSampleForPrediction = new JScrollPane();
        textAreaSampleForPrediction = new JTextArea();
        buttonUpdatePredictionData = new JButton();
        buttonChangeDataForPrediction = new JButton();
        labelNextDayPredictedTemperature = new JLabel();
        buttonPredict = new JButton();
        scrollPanePredictedTemperature = new JScrollPane();
        textAreaPredictedTemperature = new JTextArea();
        labelPredictingValuesCount2 = new JLabel();
        spinnerPredictingValuesCount2 = new JSpinner();
        panelMethodComparableAnalysis = new JPanel();
        tabbedPaneCompatibleAnalysis = new JTabbedPane();
        panelCompatibleAnalysis = new JPanel();
        buttonCompatibleAnalysis = new JButton();
        buttonUpdateComparableAnalysisData = new JButton();
        buttonChangeComparableAnalysisData = new JButton();
        labelPredictionResult = new JLabel();
        labelCompatibleAnalysisData = new JLabel();
        scrollPaneCompatibleAnalysisData = new JScrollPane();
        textAreaCompatibleAnalysisData = new JTextArea();
        scrollPaneComparativeAnalysis = new JScrollPane();
        tableComparativeAnalysis = new JTable();
        labelPredictionError = new JLabel();
        scrollPane1 = new JScrollPane();
        textAreaCompatibleAnalysisErrors = new JTextArea();
        labelPredictingValuesCount = new JLabel();
        spinnerPredictingValuesCount = new JSpinner();
        panelChartComparableAnalysis = new JPanel();
        panelPredictionMethodsErrors = new JPanel();

        //======== this ========
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);
        setTitle("\u041f\u0440\u043e\u0433\u043d\u043e\u0437\u0438\u0440\u043e\u0432\u0430\u043d\u0438\u0435 \u043f\u043e\u0433\u043e\u0434\u044b");
        Container contentPane = getContentPane();
        contentPane.setLayout(null);

        //======== tabbedPaneMain ========
        {

            //======== panelNeuralNetwork ========
            {

                // JFormDesigner evaluation mark
                panelNeuralNetwork.setBorder(new javax.swing.border.CompoundBorder(
                    new javax.swing.border.TitledBorder(new javax.swing.border.EmptyBorder(0, 0, 0, 0),
                        "JFormDesigner Evaluation", javax.swing.border.TitledBorder.CENTER,
                        javax.swing.border.TitledBorder.BOTTOM, new java.awt.Font("Dialog", java.awt.Font.BOLD, 12),
                        java.awt.Color.red), panelNeuralNetwork.getBorder())); panelNeuralNetwork.addPropertyChangeListener(new java.beans.PropertyChangeListener(){public void propertyChange(java.beans.PropertyChangeEvent e){if("border".equals(e.getPropertyName()))throw new RuntimeException();}});

                panelNeuralNetwork.setLayout(null);

                //======== scrollPaneLeaningSample ========
                {

                    //---- textAreaLeaningSample ----
                    textAreaLeaningSample.setFont(new Font("Tahoma", Font.PLAIN, 11));
                    textAreaLeaningSample.setEditable(false);
                    scrollPaneLeaningSample.setViewportView(textAreaLeaningSample);
                }
                panelNeuralNetwork.add(scrollPaneLeaningSample);
                scrollPaneLeaningSample.setBounds(10, 25, 230, 385);

                //---- labelLeaningSample ----
                labelLeaningSample.setText("\u041e\u0431\u0443\u0447\u0430\u044e\u0449\u0430\u044f \u0432\u044b\u0431\u043e\u0440\u043a\u0430");
                panelNeuralNetwork.add(labelLeaningSample);
                labelLeaningSample.setBounds(new Rectangle(new Point(10, 10), labelLeaningSample.getPreferredSize()));

                //---- labelNeuralNetworkStatus ----
                labelNeuralNetworkStatus.setText("\u0421\u0442\u0430\u0442\u0443\u0441: \u043d\u0435 \u043e\u0431\u0443\u0447\u0435\u043d\u0430");
                panelNeuralNetwork.add(labelNeuralNetworkStatus);
                labelNeuralNetworkStatus.setBounds(700, 255, 115, labelNeuralNetworkStatus.getPreferredSize().height);

                //---- buttonSaveNeuralNetwork ----
                buttonSaveNeuralNetwork.setText("\u0421\u043e\u0445\u0440\u0430\u043d\u0438\u0442\u044c \u0441\u0435\u0442\u044c");
                buttonSaveNeuralNetwork.addActionListener(e -> buttonSaveNeuralNetworkActionPerformed(e));
                panelNeuralNetwork.add(buttonSaveNeuralNetwork);
                buttonSaveNeuralNetwork.setBounds(695, 310, 115, 25);

                //======== scrollPaneNetworkData ========
                {

                    //---- textAreaNeuralNetworkLeaningCharacteristics ----
                    textAreaNeuralNetworkLeaningCharacteristics.setFont(new Font("Tahoma", Font.PLAIN, 11));
                    textAreaNeuralNetworkLeaningCharacteristics.setEditable(false);
                    scrollPaneNetworkData.setViewportView(textAreaNeuralNetworkLeaningCharacteristics);
                }
                panelNeuralNetwork.add(scrollPaneNetworkData);
                scrollPaneNetworkData.setBounds(250, 255, 430, 155);

                //---- labelNeuralNetworkParameters ----
                labelNeuralNetworkParameters.setText("\u041f\u0430\u0440\u0430\u043c\u0435\u0442\u0440\u044b \u043d\u0435\u0439\u0440\u043e\u043d\u043d\u043e\u0439 \u0441\u0435\u0442\u0438");
                panelNeuralNetwork.add(labelNeuralNetworkParameters);
                labelNeuralNetworkParameters.setBounds(new Rectangle(new Point(250, 10), labelNeuralNetworkParameters.getPreferredSize()));

                //---- buttonLoadLeaningSample ----
                buttonLoadLeaningSample.setText("\u0417\u0430\u0433\u0440\u0443\u0437\u0438\u0442\u044c \u0432\u044b\u0431\u043e\u0440\u043a\u0443");
                buttonLoadLeaningSample.addActionListener(e -> buttonLoadLeaningSampleActionPerformed(e));
                panelNeuralNetwork.add(buttonLoadLeaningSample);
                buttonLoadLeaningSample.setBounds(10, 415, 135, 25);

                //======== panelNeuralNetworkParameters ========
                {
                    panelNeuralNetworkParameters.setBorder(LineBorder.createGrayLineBorder());
                    panelNeuralNetworkParameters.setLayout(null);

                    //======== scrollPane2 ========
                    {
                        scrollPane2.setViewportView(tableNNLayers);
                    }
                    panelNeuralNetworkParameters.add(scrollPane2);
                    scrollPane2.setBounds(10, 50, 115, 105);

                    //---- spinnerNNImageDimension ----
                    spinnerNNImageDimension.setModel(new SpinnerNumberModel(5, 0, null, 1));
                    panelNeuralNetworkParameters.add(spinnerNNImageDimension);
                    spinnerNNImageDimension.setBounds(325, 10, 55, 20);

                    //---- labelNNImageDimension ----
                    labelNNImageDimension.setText("\u0420\u0430\u0437\u043c\u0435\u0440 \u043e\u0431\u0440\u0430\u0437\u0430");
                    panelNeuralNetworkParameters.add(labelNNImageDimension);
                    labelNNImageDimension.setBounds(145, 10, labelNNImageDimension.getPreferredSize().width, 20);

                    //---- spinnerNNLayersCount ----
                    spinnerNNLayersCount.setModel(new SpinnerNumberModel(1, 0, null, 1));
                    spinnerNNLayersCount.addChangeListener(e -> spinnerNNLayersCountStateChanged(e));
                    panelNeuralNetworkParameters.add(spinnerNNLayersCount);
                    spinnerNNLayersCount.setBounds(90, 10, 45, 20);

                    //---- labelNNLayersCount ----
                    labelNNLayersCount.setText("\u0427\u0438\u0441\u043b\u043e \u0443\u0440\u043e\u0432\u043d\u0435\u0439");
                    panelNeuralNetworkParameters.add(labelNNLayersCount);
                    labelNNLayersCount.setBounds(10, 10, labelNNLayersCount.getPreferredSize().width, 20);

                    //---- labelNNLayers ----
                    labelNNLayers.setText("\u0423\u0440\u043e\u0432\u043d\u0438");
                    panelNeuralNetworkParameters.add(labelNNLayers);
                    labelNNLayers.setBounds(10, 35, labelNNLayers.getPreferredSize().width, 15);

                    //---- labelNNMomentumCoeff ----
                    labelNNMomentumCoeff.setText("\u041a\u043e\u044d\u0444\u0444\u0438\u0446\u0438\u0435\u043d\u0442 \u0438\u043d\u0435\u0440\u0446\u0438\u0438 (\u043e\u0442 0 \u0434\u043e 1)");
                    panelNeuralNetworkParameters.add(labelNNMomentumCoeff);
                    labelNNMomentumCoeff.setBounds(new Rectangle(new Point(145, 40), labelNNMomentumCoeff.getPreferredSize()));

                    //---- textFieldNNMomentum ----
                    textFieldNNMomentum.addKeyListener(new KeyAdapter() {
                        @Override
                        public void keyTyped(KeyEvent e) {
                            textFieldNNMomentumKeyTyped(e);
                        }
                    });
                    panelNeuralNetworkParameters.add(textFieldNNMomentum);
                    textFieldNNMomentum.setBounds(325, 40, 55, 15);

                    //---- labelNNLeaningRate ----
                    labelNNLeaningRate.setText("\u0421\u043a\u043e\u0440\u043e\u0441\u0442\u044c \u043e\u0431\u0443\u0447\u0435\u043d\u0438\u044f (\u043e\u0442 0 \u0434\u043e 1)");
                    panelNeuralNetworkParameters.add(labelNNLeaningRate);
                    labelNNLeaningRate.setBounds(new Rectangle(new Point(145, 65), labelNNLeaningRate.getPreferredSize()));

                    //---- textFieldNNLeaningRate ----
                    textFieldNNLeaningRate.addKeyListener(new KeyAdapter() {
                        @Override
                        public void keyTyped(KeyEvent e) {
                            textFieldNNLeaningRateKeyTyped(e);
                        }
                    });
                    panelNeuralNetworkParameters.add(textFieldNNLeaningRate);
                    textFieldNNLeaningRate.setBounds(325, 65, 55, 15);

                    //---- labelNNChangingRecognitionError ----
                    labelNNChangingRecognitionError.setText("\u0418\u0437\u043c\u0435\u043d\u0435\u043d\u0438\u0435 \u043e\u0448\u0438\u0431\u043a\u0438 \u043e\u0431\u0443\u0447\u0435\u043d\u0438\u044f (%)");
                    panelNeuralNetworkParameters.add(labelNNChangingRecognitionError);
                    labelNNChangingRecognitionError.setBounds(new Rectangle(new Point(145, 90), labelNNChangingRecognitionError.getPreferredSize()));

                    //---- textFieldChangingLeaningError ----
                    textFieldChangingLeaningError.addKeyListener(new KeyAdapter() {
                        @Override
                        public void keyPressed(KeyEvent e) {
                            textFieldChangingLeaningErrorKeyPressed(e);
                        }
                    });
                    panelNeuralNetworkParameters.add(textFieldChangingLeaningError);
                    textFieldChangingLeaningError.setBounds(325, 90, 55, 15);

                    //---- buttonGetNeuralNetworkParameters ----
                    buttonGetNeuralNetworkParameters.setText("\u041f\u043e\u043b\u0443\u0447\u0438\u0442\u044c \u043f\u0430\u0440\u0430\u043c\u0435\u0442\u0440\u044b \u0441\u0435\u0442\u0438");
                    buttonGetNeuralNetworkParameters.addActionListener(e -> buttonGetNeuralNetworkParametersActionPerformed(e));
                    panelNeuralNetworkParameters.add(buttonGetNeuralNetworkParameters);
                    buttonGetNeuralNetworkParameters.setBounds(215, 130, 166, 25);
                }
                panelNeuralNetwork.add(panelNeuralNetworkParameters);
                panelNeuralNetworkParameters.setBounds(250, 25, 390, 165);

                //---- labelNeuralNetworkLeaningCharacteristics ----
                labelNeuralNetworkLeaningCharacteristics.setText("\u0425\u0430\u0440\u0430\u043a\u0442\u0435\u0440\u0438\u0441\u0442\u0438\u043a\u0438 \u043e\u0431\u0443\u0447\u0435\u043d\u0438\u044f \u043d\u0435\u0439\u0440\u043e\u043d\u043d\u043e\u0439 \u0441\u0435\u0442\u0438");
                panelNeuralNetwork.add(labelNeuralNetworkLeaningCharacteristics);
                labelNeuralNetworkLeaningCharacteristics.setBounds(new Rectangle(new Point(250, 235), labelNeuralNetworkLeaningCharacteristics.getPreferredSize()));

                //---- buttonLeanNeuralNetwork ----
                buttonLeanNeuralNetwork.setText("\u041e\u0431\u0443\u0447\u0438\u0442\u044c \u0441\u0435\u0442\u044c");
                buttonLeanNeuralNetwork.addActionListener(e -> buttonLeanNeuralNetworkActionPerformed(e));
                panelNeuralNetwork.add(buttonLeanNeuralNetwork);
                buttonLeanNeuralNetwork.setBounds(695, 280, 115, 25);

                //---- buttonCreateNeuralNetworkWithSettedParameters ----
                buttonCreateNeuralNetworkWithSettedParameters.setText("\u0421\u043e\u0437\u0434\u0430\u0442\u044c \u0441\u0435\u0442\u044c");
                buttonCreateNeuralNetworkWithSettedParameters.addActionListener(e -> buttonCreateNeuralNetworkWithSettedParametersActionPerformed(e));
                panelNeuralNetwork.add(buttonCreateNeuralNetworkWithSettedParameters);
                buttonCreateNeuralNetworkWithSettedParameters.setBounds(530, 195, 110, 25);

                //======== panelTemperatureNormalization ========
                {
                    panelTemperatureNormalization.setBorder(LineBorder.createGrayLineBorder());
                    panelTemperatureNormalization.setLayout(null);

                    //---- labelMinTemperature ----
                    labelMinTemperature.setText("\u041c\u0438\u043d\u0438\u043c\u0443\u043c");
                    panelTemperatureNormalization.add(labelMinTemperature);
                    labelMinTemperature.setBounds(new Rectangle(new Point(10, 10), labelMinTemperature.getPreferredSize()));

                    //---- labelMaxTemperature ----
                    labelMaxTemperature.setText("\u041c\u0430\u043a\u0441\u0438\u043c\u0443\u043c");
                    panelTemperatureNormalization.add(labelMaxTemperature);
                    labelMaxTemperature.setBounds(10, 35, 65, 14);

                    //---- textFieldMinTemperature ----
                    textFieldMinTemperature.addKeyListener(new KeyAdapter() {
                        @Override
                        public void keyTyped(KeyEvent e) {
                            textFieldMinTemperatureKeyTyped(e);
                        }
                    });
                    panelTemperatureNormalization.add(textFieldMinTemperature);
                    textFieldMinTemperature.setBounds(85, 10, 65, 15);

                    //---- textFieldMaxTemperature ----
                    textFieldMaxTemperature.addKeyListener(new KeyAdapter() {
                        @Override
                        public void keyTyped(KeyEvent e) {
                            textFieldMaxTemperatureKeyTyped(e);
                        }
                    });
                    panelTemperatureNormalization.add(textFieldMaxTemperature);
                    textFieldMaxTemperature.setBounds(85, 35, 65, 15);
                }
                panelNeuralNetwork.add(panelTemperatureNormalization);
                panelTemperatureNormalization.setBounds(650, 25, 165, 65);

                //---- labelTemperatureNormalization ----
                labelTemperatureNormalization.setText("\u041d\u043e\u0440\u043c\u0430\u043b\u0438\u0437\u0430\u0446\u0438\u044f \u0442\u0435\u043c\u043f\u0435\u0440\u0430\u0442\u0443\u0440\u044b");
                panelNeuralNetwork.add(labelTemperatureNormalization);
                labelTemperatureNormalization.setBounds(new Rectangle(new Point(650, 10), labelTemperatureNormalization.getPreferredSize()));

                //---- buttonSetMinAndMaxTemperature ----
                buttonSetMinAndMaxTemperature.setText("\u0417\u0430\u0434\u0430\u0442\u044c");
                buttonSetMinAndMaxTemperature.addActionListener(e -> buttonSetTemperatureNormalizationActionPerformed(e));
                panelNeuralNetwork.add(buttonSetMinAndMaxTemperature);
                buttonSetMinAndMaxTemperature.setBounds(740, 95, 75, buttonSetMinAndMaxTemperature.getPreferredSize().height);

                //---- buttonGetMinAndMaxTemperature ----
                buttonGetMinAndMaxTemperature.setText("\u041f\u043e\u043b\u0443\u0447\u0438\u0442\u044c");
                buttonGetMinAndMaxTemperature.addActionListener(e -> buttonGetTemperatureNormalizationActionPerformed(e));
                panelNeuralNetwork.add(buttonGetMinAndMaxTemperature);
                buttonGetMinAndMaxTemperature.setBounds(650, 95, 84, 23);
            }
            tabbedPaneMain.addTab("\u041d\u0435\u0439\u0440\u043e\u043d\u043d\u0430\u044f \u0441\u0435\u0442\u044c", panelNeuralNetwork);

            //======== panelNextDayPrediction ========
            {
                panelNextDayPrediction.setLayout(null);

                //---- labelNextDayPredictionData ----
                labelNextDayPredictionData.setText("\u041d\u0430\u0431\u043b\u044e\u0434\u0435\u043d\u0438\u044f \u0434\u043b\u044f \u043f\u0440\u043e\u0433\u043d\u043e\u0437\u0430");
                panelNextDayPrediction.add(labelNextDayPredictionData);
                labelNextDayPredictionData.setBounds(new Rectangle(new Point(15, 50), labelNextDayPredictionData.getPreferredSize()));

                //======== scrollPaneSampleForPrediction ========
                {

                    //---- textAreaSampleForPrediction ----
                    textAreaSampleForPrediction.setFont(new Font("Tahoma", Font.PLAIN, 11));
                    textAreaSampleForPrediction.setEditable(false);
                    scrollPaneSampleForPrediction.setViewportView(textAreaSampleForPrediction);
                }
                panelNextDayPrediction.add(scrollPaneSampleForPrediction);
                scrollPaneSampleForPrediction.setBounds(15, 70, 215, 335);

                //---- buttonUpdatePredictionData ----
                buttonUpdatePredictionData.setText("\u041e\u0431\u043d\u043e\u0432\u0438\u0442\u044c");
                buttonUpdatePredictionData.addActionListener(e -> buttonUpdateNextDayPredictionDataActionPerformed(e));
                panelNextDayPrediction.add(buttonUpdatePredictionData);
                buttonUpdatePredictionData.setBounds(110, 415, 90, 25);

                //---- buttonChangeDataForPrediction ----
                buttonChangeDataForPrediction.setText("\u0418\u0437\u043c\u0435\u043d\u0438\u0442\u044c");
                buttonChangeDataForPrediction.addActionListener(e -> buttonChangeNexDayPredictionDataActionPerformed(e));
                panelNextDayPrediction.add(buttonChangeDataForPrediction);
                buttonChangeDataForPrediction.setBounds(15, 415, 90, 25);

                //---- labelNextDayPredictedTemperature ----
                labelNextDayPredictedTemperature.setText("\u041f\u0440\u043e\u0433\u043d\u043e\u0437\u0438\u0440\u0443\u0435\u043c\u044b\u0435 \u043d\u0430\u0431\u043b\u044e\u0434\u0435\u043d\u0438\u044f");
                panelNextDayPrediction.add(labelNextDayPredictedTemperature);
                labelNextDayPredictedTemperature.setBounds(new Rectangle(new Point(255, 50), labelNextDayPredictedTemperature.getPreferredSize()));

                //---- buttonPredict ----
                buttonPredict.setText("\u041f\u0440\u043e\u0433\u043d\u043e\u0437\u0438\u0440\u043e\u0432\u0430\u0442\u044c");
                buttonPredict.addActionListener(e -> buttonNextDayPredictionActionPerformed(e));
                panelNextDayPrediction.add(buttonPredict);
                buttonPredict.setBounds(355, 415, 115, 25);

                //======== scrollPanePredictedTemperature ========
                {

                    //---- textAreaPredictedTemperature ----
                    textAreaPredictedTemperature.setFont(new Font("Tahoma", Font.PLAIN, 11));
                    scrollPanePredictedTemperature.setViewportView(textAreaPredictedTemperature);
                }
                panelNextDayPrediction.add(scrollPanePredictedTemperature);
                scrollPanePredictedTemperature.setBounds(255, 70, 215, 335);

                //---- labelPredictingValuesCount2 ----
                labelPredictingValuesCount2.setText("\u0414\u0430\u043b\u044c\u043d\u043e\u0441\u0442\u044c \u043f\u0440\u043e\u0433\u043d\u043e\u0437\u0430");
                panelNextDayPrediction.add(labelPredictingValuesCount2);
                labelPredictingValuesCount2.setBounds(15, 20, 104, 14);

                //---- spinnerPredictingValuesCount2 ----
                spinnerPredictingValuesCount2.setModel(new SpinnerNumberModel(1, 1, null, 1));
                panelNextDayPrediction.add(spinnerPredictingValuesCount2);
                spinnerPredictingValuesCount2.setBounds(125, 15, 50, 20);

                { // compute preferred size
                    Dimension preferredSize = new Dimension();
                    for(int i = 0; i < panelNextDayPrediction.getComponentCount(); i++) {
                        Rectangle bounds = panelNextDayPrediction.getComponent(i).getBounds();
                        preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                        preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                    }
                    Insets insets = panelNextDayPrediction.getInsets();
                    preferredSize.width += insets.right;
                    preferredSize.height += insets.bottom;
                    panelNextDayPrediction.setMinimumSize(preferredSize);
                    panelNextDayPrediction.setPreferredSize(preferredSize);
                }
            }
            tabbedPaneMain.addTab("\u041f\u0440\u043e\u0433\u043d\u043e\u0437\u0438\u0440\u043e\u0432\u0430\u043d\u0438\u0435", panelNextDayPrediction);

            //======== panelMethodComparableAnalysis ========
            {
                panelMethodComparableAnalysis.setLayout(null);

                //======== tabbedPaneCompatibleAnalysis ========
                {

                    //======== panelCompatibleAnalysis ========
                    {
                        panelCompatibleAnalysis.setLayout(null);

                        //---- buttonCompatibleAnalysis ----
                        buttonCompatibleAnalysis.setText("\u0421\u0440\u0430\u0432\u043d\u0438\u0442\u044c");
                        buttonCompatibleAnalysis.addActionListener(e -> buttonCompatibleAnalysisActionPerformed(e));
                        panelCompatibleAnalysis.add(buttonCompatibleAnalysis);
                        buttonCompatibleAnalysis.setBounds(250, 385, buttonCompatibleAnalysis.getPreferredSize().width, 25);

                        //---- buttonUpdateComparableAnalysisData ----
                        buttonUpdateComparableAnalysisData.setText("\u041e\u0431\u043d\u043e\u0432\u0438\u0442\u044c");
                        buttonUpdateComparableAnalysisData.addActionListener(e -> buttonUpdateComparableAnalysisDataActionPerformed(e));
                        panelCompatibleAnalysis.add(buttonUpdateComparableAnalysisData);
                        buttonUpdateComparableAnalysisData.setBounds(15, 385, buttonUpdateComparableAnalysisData.getPreferredSize().width, 25);

                        //---- buttonChangeComparableAnalysisData ----
                        buttonChangeComparableAnalysisData.setText("\u0418\u0437\u043c\u0435\u043d\u0438\u0442\u044c");
                        buttonChangeComparableAnalysisData.addActionListener(e -> buttonChangeComparableAnalysisDataActionPerformed(e));
                        panelCompatibleAnalysis.add(buttonChangeComparableAnalysisData);
                        buttonChangeComparableAnalysisData.setBounds(105, 385, 80, 25);

                        //---- labelPredictionResult ----
                        labelPredictionResult.setText("\u0420\u0435\u0437\u0443\u043b\u044c\u0442\u0430\u0442\u044b \u0441\u0440\u0430\u0432\u043d\u0438\u0442\u0435\u043b\u044c\u043d\u043e\u0433\u043e \u0430\u043d\u0430\u043b\u0438\u0437\u0430");
                        panelCompatibleAnalysis.add(labelPredictionResult);
                        labelPredictionResult.setBounds(new Rectangle(new Point(250, 10), labelPredictionResult.getPreferredSize()));

                        //---- labelCompatibleAnalysisData ----
                        labelCompatibleAnalysisData.setText("\u041d\u0430\u0431\u043b\u044e\u0434\u0435\u043d\u0438\u044f \u0434\u043b\u044f \u0441\u0440\u0430\u0432\u043d\u0438\u0442\u0435\u043b\u044c\u043d\u043e\u0433\u043e \u0430\u043d\u0430\u043b\u0438\u0437\u0430");
                        panelCompatibleAnalysis.add(labelCompatibleAnalysisData);
                        labelCompatibleAnalysisData.setBounds(new Rectangle(new Point(15, 40), labelCompatibleAnalysisData.getPreferredSize()));

                        //======== scrollPaneCompatibleAnalysisData ========
                        {

                            //---- textAreaCompatibleAnalysisData ----
                            textAreaCompatibleAnalysisData.setEditable(false);
                            textAreaCompatibleAnalysisData.setFont(new Font("Tahoma", Font.PLAIN, 11));
                            scrollPaneCompatibleAnalysisData.setViewportView(textAreaCompatibleAnalysisData);
                        }
                        panelCompatibleAnalysis.add(scrollPaneCompatibleAnalysisData);
                        scrollPaneCompatibleAnalysisData.setBounds(15, 60, 215, 315);

                        //======== scrollPaneComparativeAnalysis ========
                        {
                            scrollPaneComparativeAnalysis.setViewportView(tableComparativeAnalysis);
                        }
                        panelCompatibleAnalysis.add(scrollPaneComparativeAnalysis);
                        scrollPaneComparativeAnalysis.setBounds(250, 25, 575, 245);

                        //---- labelPredictionError ----
                        labelPredictionError.setText("\u041e\u0448\u0438\u0431\u043a\u0438 \u043f\u0440\u043e\u0433\u043d\u043e\u0437\u0438\u0440\u043e\u0432\u0430\u043d\u0438\u044f");
                        panelCompatibleAnalysis.add(labelPredictionError);
                        labelPredictionError.setBounds(new Rectangle(new Point(250, 280), labelPredictionError.getPreferredSize()));

                        //======== scrollPane1 ========
                        {

                            //---- textAreaCompatibleAnalysisErrors ----
                            textAreaCompatibleAnalysisErrors.setFont(new Font("Tahoma", Font.PLAIN, 11));
                            textAreaCompatibleAnalysisErrors.setEditable(false);
                            scrollPane1.setViewportView(textAreaCompatibleAnalysisErrors);
                        }
                        panelCompatibleAnalysis.add(scrollPane1);
                        scrollPane1.setBounds(250, 295, 280, 80);

                        //---- labelPredictingValuesCount ----
                        labelPredictingValuesCount.setText("\u0414\u0430\u043b\u044c\u043d\u043e\u0441\u0442\u044c \u043f\u0440\u043e\u0433\u043d\u043e\u0437\u0430");
                        panelCompatibleAnalysis.add(labelPredictingValuesCount);
                        labelPredictingValuesCount.setBounds(15, 15, 104, 14);

                        //---- spinnerPredictingValuesCount ----
                        spinnerPredictingValuesCount.setModel(new SpinnerNumberModel(1, 1, null, 1));
                        panelCompatibleAnalysis.add(spinnerPredictingValuesCount);
                        spinnerPredictingValuesCount.setBounds(125, 10, 50, 20);

                        { // compute preferred size
                            Dimension preferredSize = new Dimension();
                            for(int i = 0; i < panelCompatibleAnalysis.getComponentCount(); i++) {
                                Rectangle bounds = panelCompatibleAnalysis.getComponent(i).getBounds();
                                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                            }
                            Insets insets = panelCompatibleAnalysis.getInsets();
                            preferredSize.width += insets.right;
                            preferredSize.height += insets.bottom;
                            panelCompatibleAnalysis.setMinimumSize(preferredSize);
                            panelCompatibleAnalysis.setPreferredSize(preferredSize);
                        }
                    }
                    tabbedPaneCompatibleAnalysis.addTab("\u0421\u0440\u0430\u0432\u043d\u0438\u0442\u0435\u043b\u044c\u043d\u044b\u0439 \u0430\u043d\u0430\u043b\u0438\u0437", panelCompatibleAnalysis);

                    //======== panelChartComparableAnalysis ========
                    {
                        panelChartComparableAnalysis.setLayout(null);

                        { // compute preferred size
                            Dimension preferredSize = new Dimension();
                            for(int i = 0; i < panelChartComparableAnalysis.getComponentCount(); i++) {
                                Rectangle bounds = panelChartComparableAnalysis.getComponent(i).getBounds();
                                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                            }
                            Insets insets = panelChartComparableAnalysis.getInsets();
                            preferredSize.width += insets.right;
                            preferredSize.height += insets.bottom;
                            panelChartComparableAnalysis.setMinimumSize(preferredSize);
                            panelChartComparableAnalysis.setPreferredSize(preferredSize);
                        }
                    }
                    tabbedPaneCompatibleAnalysis.addTab("\u0413\u0440\u0430\u0444\u0438\u043a \u0441\u0440\u0430\u0432\u043d\u0438\u0442\u0435\u043b\u044c\u043d\u043e\u0433\u043e \u0430\u043d\u0430\u043b\u0438\u0437\u0430", panelChartComparableAnalysis);

                    //======== panelPredictionMethodsErrors ========
                    {
                        panelPredictionMethodsErrors.setLayout(null);

                        { // compute preferred size
                            Dimension preferredSize = new Dimension();
                            for(int i = 0; i < panelPredictionMethodsErrors.getComponentCount(); i++) {
                                Rectangle bounds = panelPredictionMethodsErrors.getComponent(i).getBounds();
                                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                            }
                            Insets insets = panelPredictionMethodsErrors.getInsets();
                            preferredSize.width += insets.right;
                            preferredSize.height += insets.bottom;
                            panelPredictionMethodsErrors.setMinimumSize(preferredSize);
                            panelPredictionMethodsErrors.setPreferredSize(preferredSize);
                        }
                    }
                    tabbedPaneCompatibleAnalysis.addTab("\u041e\u0448\u0438\u0431\u043a\u0430 \u043f\u0440\u043e\u0433\u043d\u043e\u0437\u0438\u0440\u043e\u0432\u0430\u043d\u0438\u044f", panelPredictionMethodsErrors);
                }
                panelMethodComparableAnalysis.add(tabbedPaneCompatibleAnalysis);
                tabbedPaneCompatibleAnalysis.setBounds(0, 0, 880, 445);

                { // compute preferred size
                    Dimension preferredSize = new Dimension();
                    for(int i = 0; i < panelMethodComparableAnalysis.getComponentCount(); i++) {
                        Rectangle bounds = panelMethodComparableAnalysis.getComponent(i).getBounds();
                        preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                        preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                    }
                    Insets insets = panelMethodComparableAnalysis.getInsets();
                    preferredSize.width += insets.right;
                    preferredSize.height += insets.bottom;
                    panelMethodComparableAnalysis.setMinimumSize(preferredSize);
                    panelMethodComparableAnalysis.setPreferredSize(preferredSize);
                }
            }
            tabbedPaneMain.addTab("\u0421\u0440\u0430\u0432\u043d\u0435\u043d\u0438\u0435 \u043c\u0435\u0442\u043e\u0434\u043e\u0432", panelMethodComparableAnalysis);
        }
        contentPane.add(tabbedPaneMain);
        tabbedPaneMain.setBounds(0, 5, 840, 480);

        { // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < contentPane.getComponentCount(); i++) {
                Rectangle bounds = contentPane.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = contentPane.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            contentPane.setMinimumSize(preferredSize);
            contentPane.setPreferredSize(preferredSize);
        }
        setSize(840, 510);
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Oleg K
    private JTabbedPane tabbedPaneMain;
    private JPanel panelNeuralNetwork;
    private JScrollPane scrollPaneLeaningSample;
    private JTextArea textAreaLeaningSample;
    private JLabel labelLeaningSample;
    private JLabel labelNeuralNetworkStatus;
    private JButton buttonSaveNeuralNetwork;
    private JScrollPane scrollPaneNetworkData;
    private JTextArea textAreaNeuralNetworkLeaningCharacteristics;
    private JLabel labelNeuralNetworkParameters;
    private JButton buttonLoadLeaningSample;
    private JPanel panelNeuralNetworkParameters;
    private JScrollPane scrollPane2;
    private JTable tableNNLayers;
    private JSpinner spinnerNNImageDimension;
    private JLabel labelNNImageDimension;
    private JSpinner spinnerNNLayersCount;
    private JLabel labelNNLayersCount;
    private JLabel labelNNLayers;
    private JLabel labelNNMomentumCoeff;
    private JTextField textFieldNNMomentum;
    private JLabel labelNNLeaningRate;
    private JTextField textFieldNNLeaningRate;
    private JLabel labelNNChangingRecognitionError;
    private JTextField textFieldChangingLeaningError;
    private JButton buttonGetNeuralNetworkParameters;
    private JLabel labelNeuralNetworkLeaningCharacteristics;
    private JButton buttonLeanNeuralNetwork;
    private JButton buttonCreateNeuralNetworkWithSettedParameters;
    private JPanel panelTemperatureNormalization;
    private JLabel labelMinTemperature;
    private JLabel labelMaxTemperature;
    private JTextField textFieldMinTemperature;
    private JTextField textFieldMaxTemperature;
    private JLabel labelTemperatureNormalization;
    private JButton buttonSetMinAndMaxTemperature;
    private JButton buttonGetMinAndMaxTemperature;
    private JPanel panelNextDayPrediction;
    private JLabel labelNextDayPredictionData;
    private JScrollPane scrollPaneSampleForPrediction;
    private JTextArea textAreaSampleForPrediction;
    private JButton buttonUpdatePredictionData;
    private JButton buttonChangeDataForPrediction;
    private JLabel labelNextDayPredictedTemperature;
    private JButton buttonPredict;
    private JScrollPane scrollPanePredictedTemperature;
    private JTextArea textAreaPredictedTemperature;
    private JLabel labelPredictingValuesCount2;
    private JSpinner spinnerPredictingValuesCount2;
    private JPanel panelMethodComparableAnalysis;
    private JTabbedPane tabbedPaneCompatibleAnalysis;
    private JPanel panelCompatibleAnalysis;
    private JButton buttonCompatibleAnalysis;
    private JButton buttonUpdateComparableAnalysisData;
    private JButton buttonChangeComparableAnalysisData;
    private JLabel labelPredictionResult;
    private JLabel labelCompatibleAnalysisData;
    private JScrollPane scrollPaneCompatibleAnalysisData;
    private JTextArea textAreaCompatibleAnalysisData;
    private JScrollPane scrollPaneComparativeAnalysis;
    private JTable tableComparativeAnalysis;
    private JLabel labelPredictionError;
    private JScrollPane scrollPane1;
    private JTextArea textAreaCompatibleAnalysisErrors;
    private JLabel labelPredictingValuesCount;
    private JSpinner spinnerPredictingValuesCount;
    private JPanel panelChartComparableAnalysis;
    private JPanel panelPredictionMethodsErrors;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
