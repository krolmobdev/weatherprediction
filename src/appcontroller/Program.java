package appcontroller;

import javax.swing.*;

public class Program {
    public static final String NAME = "Прогнозирование погоды";

    public static void main(String[] args) {

        // устанавливаем LookAndFeel
        initSystemLookAndFeel();

        new MainForm();
    }

    /**
     * Устанавливает тему приложения по умолчанию
     */
    private static void initSystemLookAndFeel() {
        try {
            String systemLookAndFeelClassName = UIManager.getSystemLookAndFeelClassName();
            // устанавливаем LookAndFeel
            UIManager.setLookAndFeel(systemLookAndFeelClassName);
        } catch (UnsupportedLookAndFeelException e) {
            System.err.println("Can't use the specified look and feel on this platform.");
        } catch (Exception e) {
            System.err.println("Couldn't get specified look and feel, for some reason.");
        }
    }
}
