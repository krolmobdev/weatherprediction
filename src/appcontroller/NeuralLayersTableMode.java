package appcontroller;

import javax.swing.table.AbstractTableModel;
import java.util.List;

/**
 * Описывает модель таблицы для отображения числа нейронов на уровнях сети
 */
public class NeuralLayersTableMode extends AbstractTableModel {
    private List<NeuralLayerTableItem> mNeuralLayers;//Уровни нейронной сети

    /**
     * Создает модель таблицы
     *
     * @param neuralLayers Уровни нейронной сети
     */
    public NeuralLayersTableMode(List<NeuralLayerTableItem> neuralLayers) {
        mNeuralLayers = neuralLayers;
    }

    /**
     * Возвращает уровни нейронной сети
     */
    public List<NeuralLayerTableItem> getNeuralLayers() {
        return mNeuralLayers;
    }

    @Override
    public String getColumnName(int column) {
        String result = "";
        switch (column) {
            case 0: {
                result = "№";
                break;
            }
            case 1: {
                result = "Число нейронов";
                break;
            }
        }
        return result;
    }

    @Override
    public int getRowCount() {
        return mNeuralLayers.size();
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0: {
                return mNeuralLayers.get(rowIndex).getNumber() + 1;
            }
            case 1: {
                return mNeuralLayers.get(rowIndex).getNeuroneCount();
            }
            default: {
                return "";
            }
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        boolean result = false;
        switch (columnIndex) {
            case 0: {
                result = false;
                break;
            }
            case 1: {
                result = rowIndex != mNeuralLayers.size() - 1;
                break;
            }
        }
        return result;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        if (columnIndex == 1) {
            mNeuralLayers.get(rowIndex).setNeuroneCount(Integer.parseInt((String) aValue));
        }
    }


}
