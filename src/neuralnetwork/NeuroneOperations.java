package neuralnetwork;

/**
 * Описывает математические операции нейрона
 */
public final class NeuroneOperations {
    private NeuroneOperations() {
    }

    /**
     * Изменение веса нейрона
     *
     * @param leaningRate          Коэффициент обучения
     * @param momentum             Коэффициент инертности
     * @param input                Входное значение нейрона
     * @param weightError          Ошибка веса
     * @param prevWeightDifference Предыдущая разница веса
     */
    public static double weightDifference(double leaningRate, double momentum, double input,
                                          double weightError, double prevWeightDifference) {

        return leaningRate * weightError * input + momentum * prevWeightDifference;
    }

    /**
     * Находит выходное значение нейрона
     *
     * @param sum Взвешенная сумма нейрона
     */
    public static double out(double sum) {
        return 1 / (1 + (Math.exp(-2*sum)));
    }

    /**
     * Ошибка нейрона последнего уровня
     *
     * @param out                        Выходное значение нейрона
     * @param weightedErrorChildNeurones Суммарная взвешенная ошибка с дочерними нейронами
     */
    public static double errorNotLastLayerNeurone(double out, double weightedErrorChildNeurones) {
        return out * (1 - out) * weightedErrorChildNeurones;
    }

    /**
     * Ошибка нейрона непоследнего уровня
     *
     * @param out        Выходное значение нейрона
     * @param correctOut Правильное выходное значение
     */
    public static double errorLastLayerNeurone(double out, double correctOut) {
        return out * (1 - out) * (correctOut - out);
    }
}
