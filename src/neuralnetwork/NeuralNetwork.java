package neuralnetwork;

import java.io.*;
import java.util.*;

/**
 * Нейронная сеть
 */
public class NeuralNetwork implements Serializable {
    private static final long serialVersionUID = -2058276278521140107L;

    private double mMomentum = 0.1;//коэффициент инертности
    private double mLeaningRate = 0.9;//скорость обучения
    private double mChangingLeaningError = 0.00001;//0.01 % по умолчанию

    private int mEpochCount;//число эпох обучения

    private int mImageDimension = 1;//размерность распознаваемого образа
    private int mLayersCount;//число уровней сети
    private List<NeuralLayer> mLayers = new ArrayList<>();//уровни сети

    private double mLeaningError;//ошибка обучения сети
    private double mLeaningErrorDifference;//разница ошибки обучения


    /**
     * Создает нейронную сеть с нулевым числом уровней  для распознавания образа размерностью imageDimension
     * с коэффициентом инертности 0.1 и скоростью обучения 0.9
     *
     * @param imageDimension Размерность распознаваемого образа
     */
    public NeuralNetwork(int imageDimension) {
        if (imageDimension <= 0) {
            throw new IllegalArgumentException("Значение параметра imageDimension меньше нуля");
        }
        mImageDimension = imageDimension;
    }

    /**
     * Сериализует объект сети в файл
     *
     * @param serFilename Имя файла
     * @param network     Объект нейронной сети
     * @throws IOException При возникновении ошибки записи объекта в файл
     */
    public static void saveNetwork(String serFilename, NeuralNetwork network) throws IOException {
        ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(serFilename));
        os.writeObject(network);
        os.close();
    }

    /**
     * Востанавливает сериализованный объект сети из файла
     *
     * @param serFilename Имя файла
     * @throws IOException При возникновении ошибки чтения объекта из файла
     */
    public static NeuralNetwork loadNetwork(String serFilename) throws IOException {
        try {
            if (!new File(serFilename).exists()) {
                throw new IOException("Отсутствует файл " + serFilename + " с сохраненной сетью");
            }

            ObjectInputStream os = new ObjectInputStream(new FileInputStream(serFilename));
            NeuralNetwork network = (NeuralNetwork) os.readObject();
            os.close();

            return network;
        } catch (ClassNotFoundException ex) {
            throw new IOException(ex);
        }
    }

    /**
     * Добавляет новый уровень нейронов в сеть
     *
     * @param neuroneCount Число нейронов на уровне
     * @throws IllegalArgumentException Если neuroneCount<1
     */
    public void addLayer(int neuroneCount) {
        if (neuroneCount <= 0) {
            throw new IllegalArgumentException("Значение параметра neuroneCount меньше 1");
        }
        //Создаем первый уровень сети с числом весовых коэффициентов, равных размерности распознаваемого образа
        if (mLayersCount == 0) {
            mLayers.add(new NeuralLayer(neuroneCount, mImageDimension));

        } else {//Создаем следующий уровень сети c числом весовых коэффициентов, равных числу нейронов родительского уровня
            //Число нейронов на родительском уровне
            int prevLayerNeuroneCount = mLayers.get(mLayersCount - 1).getNeurones().size();

            mLayers.add(new NeuralLayer(neuroneCount, prevLayerNeuroneCount));
        }
        mLayersCount++;
    }

    /**
     * Возвращает размерность распознаваемого образа
     *
     * @return Размерность распознаваемого образа
     */
    public int getImageDimension() {
        return mImageDimension;
    }

    /**
     * Возвращает число уровней сети
     *
     * @return Число уровней сети
     */
    public int getLayersCount() {
        return mLayersCount;
    }

    /**
     * Возвращает уровни нейронной сети
     *
     * @return Уровни нейронной сети
     */
    public List<NeuralLayer> getLayers() {
        return mLayers;
    }

    /**
     * Возвращает число эпох обучения
     *
     * @return Число эпох обучения
     */
    public int getEpochCount() {
        return mEpochCount;
    }

    /**
     * Возвращает ошибку обучения сети
     *
     * @return Значение ошибки обучения сети
     */
    public double getLeaningError() {
        return mLeaningError;
    }

    /**
     * Возвращает значение момента инерции
     *
     * @return момент инерции
     */
    public double getMomentum() {
        return mMomentum;
    }

    /**
     * Устанавливает значение момента инерции
     *
     * @param momentum Момент инерции
     */
    public void setMomentum(double momentum) {
        mMomentum = momentum;
    }

    /**
     * Возвращает значение скорости обучения
     *
     * @return Скорость обучения
     */
    public double getLeaningRate() {
        return mLeaningRate;
    }

    /**
     * Устанавливает значение скорости обучения
     *
     * @param leaningRate Скорость обучения
     */
    public void setLeaningRate(double leaningRate) {
        mLeaningRate = leaningRate;
    }

    /**
     * Возвращает интенсивность изменения среднеквадратичной ошибки
     *
     * @return Интенсивность изменения среднеквадратичной ошибки
     */
    public double getChangingLeaningError() {
        return mChangingLeaningError;
    }

    /**
     * Устанавливает интенсивность изменения среднеквадратичной ошибки
     *
     * @param changingLeaningError Интенсивность изменения среднеквадратичной ошибки
     */
    public void setChangingLeaningError(double changingLeaningError) {
        mChangingLeaningError = changingLeaningError;
    }

    public boolean isEmpty() {
        return mLayersCount == 0;
    }

    /**
     * Обучить сеть
     *
     * @param sample                Выборка.
     *                              Представляет отображение вида: обучающий образ -> правильное значение.
     */
    public void lean(Map<List<Double>, List<Double>> sample) {

        if (mLayersCount == 0) {
            return;
        }

        //Формируем обучающую и контрольную части выборки

        //Выборка: множество пар: образ -> выходное значение
        Set<Map.Entry<List<Double>, List<Double>>> setSample = sample.entrySet();

        //Обучающая часть выборки
        Set<Map.Entry<List<Double>, List<Double>>> trainingPart = new LinkedHashSet<Map.Entry<List<Double>, List<Double>>>();
        //Контрольная часть выборки
        Set<Map.Entry<List<Double>, List<Double>>> controlPart = new LinkedHashSet<Map.Entry<List<Double>, List<Double>>>();

        int controlPartSize = (int) (setSample.size() * 0.75);

        int i1 = 0;
        for (Map.Entry<List<Double>, List<Double>> item : setSample) {
            if (i1 <= controlPartSize - 1) {
                trainingPart.add(item);
            } else {
                controlPart.add(item);
            }
            i1++;
        }

        //Инициализируем список ошибок распознавания элементов контрольной выборки

        List<Double> trainingSamplesError = new ArrayList<Double>(controlPart.size());
        for (int i = 0; i < controlPart.size(); i++) {
            trainingSamplesError.add(Double.POSITIVE_INFINITY);
        }

        boolean isEndLeaning;

        do {
            isEndLeaning = false;

            //Обучаем сеть на элементах обучающей выборки в течение 100 эпох

            for (Map.Entry<List<Double>, List<Double>> item : trainingPart) {
                List<Double> image = item.getKey();
                List<Double> correctOutputs = item.getValue();

                recognize(image);
                backPropagationErrorAlgorithm(correctOutputs);
            }
            //Logger.getLogger(getClass().getCanonicalName()).log(Level.INFO, "\n\nЭпоха №" + mEpochCount + "\n\nСеть:" + this);

            mEpochCount++;

            //Если прошли очередные 100 эпох обучения,

            //if (mEpochCount % 5 == 0) {

            isEndLeaning = true;

            //Вычисляем ошибки обучения на контрольной выборке

            int i2 = 0;
            for (Map.Entry<List<Double>, List<Double>> item : controlPart) {
                List<Double> image = item.getKey();
                List<Double> correctOutputs = item.getValue();

                recognize(image);

                double error = calculateRecognizingError(correctOutputs);
                trainingSamplesError.set(i2, error);

                //Условие продолжения обучения
                //if (error >= allowableLeaningError) isEndLeaning = false;

                i2++;
            }

            //Вычисляем среднеквадратичную ошибку обучения за эпоху

            double curLeaningError = 0.0;
            for (Double sampleError : trainingSamplesError) {
                curLeaningError += sampleError;
            }
            curLeaningError /= trainingSamplesError.size();

            //Вычисляем изменение средней ошибки обучения

            mLeaningErrorDifference = Math.abs(mLeaningError - curLeaningError);
            mLeaningError = curLeaningError;

            //Условие продолжения процесса обучения:
            // если изменение среднеквадратичной ошибки обучения за эпоху больше заданного значения
            if (mLeaningErrorDifference / mLeaningError > mChangingLeaningError) isEndLeaning = false;
            //}
        } while (!isEndLeaning);
    }

    /**
     * Вычисление ошибки нейронов по алгоритму обратного распространения и корректировка весовых коэффициентов
     *
     * @param correctOutputs правильные выходные значения
     */
    private void backPropagationErrorAlgorithm(List<Double> correctOutputs) {

        //В направлении от последнего уровня к первому вычисляем значение ошибки каждого нейрона
        for (int layer = mLayersCount - 1; layer >= 0; layer--) {
            //Последний уровень сети
            if (layer == mLayersCount - 1) {

                mLayers.get(layer).calculateNeuronesErrorLastLayer(correctOutputs);

            } else {//Непоследний уровень сети

                mLayers.get(layer).calculateNeuronesErrorNotLastLayer(mLayers.get(layer + 1));
            }
        }

        for (NeuralLayer layer : mLayers) {
            layer.updateWeights(mLeaningRate, mMomentum);
        }
    }

    /**
     * Вычисляет среднюю ошибку распознавания сети по формуле среднеквадратичной ошибки
     *
     * @param correctOutputs Правильные выходные значения сети
     */
    public double calculateRecognizingError(List<Double> correctOutputs) {
        if (correctOutputs == null || correctOutputs.size() != mLayers.get(mLayersCount - 1).getNeuroneCount()) {
            throw new IllegalArgumentException("Параметр correctOutputs равен null");
        }
        if (correctOutputs.size() != mLayers.get(mLayersCount - 1).getNeuroneCount()) {
            throw new NeuralNetworkRuntimeException("Число правильных значений выходов correctOutputs не равно числу выходов");
        }

        double recognizingError = 0.0;

        int i = 0;
        for (Neurone neurone : mLayers.get(mLayersCount - 1).getNeurones()) {
            recognizingError += Math.pow((correctOutputs.get(i) - neurone.getOut()), 2);
            i++;
        }
        recognizingError /= 2;

        return recognizingError;
    }

    /**
     * Распознать входной образ
     *
     * @param image Распознаваемый образ
     * @return Выходные значения
     */
    public List<Double> recognize(List<Double> image) {
        if (mLayersCount == 0) {
            throw new NeuralNetworkRuntimeException("Число уровней сети равно нулю");
        }

        if (image == null) {
            throw new IllegalArgumentException("Параметр image равен null");
        }
        if (image.size() != mImageDimension) {
            throw new NeuralNetworkRuntimeException("Размерность образа image не равна размерности образа, которую распознает сеть");
        }

        List<Double> layerData = new ArrayList<Double>(image);

        //Задаем входные значения для нейронов каждого слоя,
        //вычисляем выходные значения и передаем их нейронам следующего слоя
        for (NeuralLayer layer : mLayers) {
            layer.setNeuronesInput(layerData);

            layer.calculateNeuronesOutput();
            layerData = layer.getNeuronesOut();
        }

        //Logger.getLogger(getClass().getCanonicalName()).log(Level.INFO, "\n\nСеть:" + this);

        return layerData;
    }

    @Override
    public String toString() {
        String res = "Размерность образа " + mImageDimension +
                "\nКоэффициент инертности " + mMomentum +
                "\nСкорость обучения " + mLeaningRate +
                "\nОшибка обучения " + mLeaningError +
                "\nИзменение ошибки обучения " + mLeaningErrorDifference +
                "\nЧисло эпох обучения " + mEpochCount +
                "\nЧисло уровней " + mLayersCount +
                "\n\nУровни";

        StringBuilder resBuilder = new StringBuilder(res);

        int i = 1;
        for (NeuralLayer layer : mLayers) {
            resBuilder.append('\n');
            resBuilder.append(i);
            resBuilder.append(' ');
            resBuilder.append(layer.toString());
            i++;
        }
        resBuilder.append('\n');
        return resBuilder.toString();
    }

    /**
     * Строковое представление сети для записи в текстовый файл:
     * 1: размерность распознаваемого образа
     * 2: коэффициент инертности
     * 3: скорость обучения
     * 4: ошибка обучения
     * 5: изменение ошибки обучения
     * 6: число эпох обучения
     * 7: число уровней
     * 8 и т.д - уровни в формате:
     * 1: номер_уровня число_нейронов
     * 2: и т.д - нейроны уровня в формате:
     * 1: номер_нейрона число_весов
     * 2 и т.д: веса в формате: вес1 вес2...весn
     */
    public String toTextFile() {

        String result = "";

        StringBuilder sb = new StringBuilder(result);
        sb.append(mImageDimension);
        sb.append("\r\n");
        sb.append(mMomentum);
        sb.append("\r\n");
        sb.append(mLeaningRate);
        sb.append("\r\n");
        sb.append(mLeaningError);
        sb.append("\r\n");
        sb.append(mLeaningErrorDifference);
        sb.append("\r\n");
        sb.append(mEpochCount);
        sb.append("\r\n");
        sb.append(mLayersCount);

        //Строковое представление уровней
        for (int i = 0; i < mLayersCount; i++) {
            sb.append("\r\n");
            sb.append(i);
            sb.append(' ');
            sb.append(mLayers.get(i).toTextFile());
        }
        return sb.toString();
    }

    /**
     * Сохраняет сеть в текстовый файл
     *
     * @param file Текстовый файл
     * @throws IOException              При ошибке записи сети в файл
     * @throws IllegalArgumentException Если агрумент file равен null
     */
    public void saveToTextFile(File file) throws IOException {
        if (file == null) {
            throw new IllegalArgumentException("Значение параметра file равно null");
        }

        BufferedWriter writer = new BufferedWriter(new FileWriter(file));
        writer.write(toTextFile());
        writer.close();
    }
}
