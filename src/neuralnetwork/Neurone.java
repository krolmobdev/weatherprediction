package neuralnetwork;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/***
 * Описывает нейрон
 */
public class Neurone implements Serializable{
    private static final long serialVersionUID = -3293599844521016475L;

    public static final double MIN_START_WEIGHT_VALUE = 0.01;
    public static final double MAX_START_WEIGHT_VALUE = 0.02;

    transient private double mOut;//реакция нейрона
    transient private double mError;//значение ошибки
    private int mWeightsCount;//число весов
    private List<Double> mWeights;//веса нейрона
    private List<Double> mWeightsDifference;//разница весов

    /***
     * Конструктор: создание нейрона
     * @param neuronesPreviousLayerCount Число нейронов на предшествующем уровне
     */
    public Neurone(int neuronesPreviousLayerCount) {
        if (neuronesPreviousLayerCount < 0) {
            throw new IllegalArgumentException("Параметр weightCount должен быть больше нуля");
        }

        mWeightsCount = neuronesPreviousLayerCount;

        mWeights = new ArrayList<Double>(mWeightsCount);
        randomizeWeights();

        mWeightsDifference = new ArrayList<Double>(mWeightsCount);

        for (int i = 0; i < mWeightsCount; i++) {
            mWeightsDifference.add(0.0);
        }
    }

    /**
     * Создает нейрон из массива строк формата
     * 1: число_весов
     * 2 и т.д: веса в формате: вес1 вес2...весn
     *
     * @param data Данные нейрона
     */
    public Neurone(List<String> data){
        mWeightsCount = Integer.parseInt(data.get(0));

        mWeights = new ArrayList<Double>(mWeightsCount);
        //Разбираем данные о нейронах
        for (String weight:data.get(1).split(" ")){
            mWeights.add(Double.parseDouble(weight));
        }

        mWeightsDifference = new ArrayList<Double>(mWeightsCount);
        for (int i = 0; i < mWeightsCount; i++) {
            mWeightsDifference.add(0.0);
        }
    }

    private void parseNeuroneData(List<String> data) {
        mWeightsCount = Integer.parseInt(data.get(0));

        //Разбираем данные об уровнях сети
        for (String line : data) {

        }
    }

    /**
     * Генерируем случайные значения весов от 0.01 до 0.02
     */
    private void randomizeWeights() {
        for (int i = 0; i < mWeightsCount; i++) {
            mWeights.add(MIN_START_WEIGHT_VALUE +
                    Math.random() * (MAX_START_WEIGHT_VALUE - MIN_START_WEIGHT_VALUE));
        }
    }

    /**
     * Получение реакции нейрона
     *
     * @return реакция нейрона
     */
    public Double getOut() {
        return mOut;
    }

    /**
     * Задание реакции нейрона
     *
     * @param out Реакция нейрона
     */
    public void setOut(Double out) {
        mOut = out;
    }

    /**
     * Получение значения ошибки
     *
     * @return Значение ошибки
     */
    public Double getError() {
        return mError;
    }

    /**
     * Задание значения ошибки
     *
     * @param error Значение ошибки
     */
    public void setError(Double error) {
        mError = error;
    }

    /**
     * Получение количества весов
     *
     * @return Количество весов
     */
    public int getWeightsCount() {
        return mWeightsCount;
    }

    /**
     * Получение весов нейрона
     *
     * @return Веса нейрона
     */
    public List<Double> getWeights() {
        return mWeights;
    }

    /**
     * Получить изменения весов
     *
     * @return Список изменений весов
     */
    public List<Double> getWeightsDifference() {
        return mWeightsDifference;
    }

    @Override
    public String toString() {
        String res="нейрон" +
                "\nЧисло весов " + mWeightsCount +
                "\nВеса:"+mWeights;

        return res;
    }

    /**
     * Строковое представление нейрона для записи в файл
     * 1 число_весов
     * 2 и т.д: веса в формате: вес1 вес2...весn
     */
    public String toTextFile(){
        String result="";

        StringBuilder sb=new StringBuilder(result);
        sb.append(mWeightsCount);
        sb.append("\r\n");

        //Строковое представление весовых коэффициентов
        for (Double weight:getWeights()) {
            sb.append(weight);
            sb.append(' ');
        }
        sb.deleteCharAt(sb.length()-1);

        return sb.toString();
    }
}
