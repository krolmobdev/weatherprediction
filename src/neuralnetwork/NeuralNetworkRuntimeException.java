package neuralnetwork;

/**
 * Описывает непроверямое исключение в процессе работы нейронной сети
 */
public class NeuralNetworkRuntimeException extends RuntimeException {
    public NeuralNetworkRuntimeException() {
        super();
    }

    public NeuralNetworkRuntimeException(String message) {
        super(message);
    }
}
