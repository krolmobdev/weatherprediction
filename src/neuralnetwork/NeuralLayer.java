package neuralnetwork;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Описывает уровень сети
 */
public class NeuralLayer implements Serializable {
    private static final long serialVersionUID = 5902174571627730966L;

    private int mNeuroneCount;//число нейронов
    private List<Neurone> mNeurones;//нейроны
    transient private List<Double> mNeuronesInput=new ArrayList<Double>();//входные значения нейронов

    /**
     * Конструктор: создание уровня нейронов.
     *
     * @param neuroneCount        Число нейронов уровня
     * @param neuroneWeightsCount Число весовых коэффициентов каждого нейрона
     * @throws IllegalArgumentException Если параметр mNeuroneCount<=0
     */
    public NeuralLayer(int neuroneCount, int neuroneWeightsCount) {
        if (neuroneCount <= 0) {
            throw new IllegalArgumentException("Параметр mNeuroneCount должен быть больше нуля");
        }
        mNeuroneCount = neuroneCount;

        mNeurones = new ArrayList<Neurone>(neuroneCount);
        for (int i = 0; i < neuroneCount; i++) {
            mNeurones.add(new Neurone(neuroneWeightsCount));
        }
    }

    /**
     * Разбирает данные о нейронном уровне из массива строк:
     * 1: номер уровня число_нейронов
     * 2: и т.д - нейроны уровня в формате:
     * 1: номер_нейрона число_весов
     * 2 и т.д: веса в формате: вес1 вес2...весn
     *
     * @param data Данные уровня сети
     */
    private void parseLayerData(List<String> data) {
        String[] splitData=data.get(0).split(" ");
        mNeuroneCount = Integer.parseInt(splitData[1]);
    }

    /**
     * Получение числа нейронов уровня
     *
     * @return Число нейронов уровня
     */
    public int getNeuroneCount() {
        return mNeuroneCount;
    }

    /**
     * Получение нейронов уровня
     *
     * @return Список нейронов уровня
     */
    public List<Neurone> getNeurones() {
        return mNeurones;
    }

    /**
     * Получение входных значений нейронов данного слоя
     *
     * @return Входные значения нейронов данного слоя
     */
    public List<Double> getNeuronesInput() {
        return mNeuronesInput;
    }

    /**
     * Задание входных значений нейронов уровня
     *
     * @param neuronesInput Входные значения нейронов уровня
     * @throws IllegalArgumentException Если mNeuronesInput равен null
     */
    public void setNeuronesInput(List<Double> neuronesInput) {
        if (neuronesInput == null) {
            throw new IllegalArgumentException("Параметр mNeuronesInput равен null");
        }

        mNeuronesInput = neuronesInput;
    }

    /**
     * Находит выходные значения нейронов уровня
     */
    public void calculateNeuronesOutput() {
        for (Neurone neurone : mNeurones) {

            double sum = 0.0;
            for (double weight : neurone.getWeights()) {
                sum += weight * mNeuronesInput.get(neurone.getWeights().indexOf(weight));
            }
            neurone.setOut(NeuroneOperations.out(sum));
        }
    }

    /**
     * Обновляем весовые коэффициенты
     *
     * @param leaningRate Скорость обучения
     * @param momentum  Коэффициент инерциональности
     */
    public void updateWeights(final double leaningRate, final double momentum) {
        for (Neurone neurone : mNeurones) {
            List<Double> weights = neurone.getWeights();
            List<Double> weightsDiff = neurone.getWeightsDifference();

            for (int i = 0; i < neurone.getWeightsCount(); i++) {
                double weightDiff = NeuroneOperations.weightDifference(leaningRate, momentum,
                                    mNeuronesInput.get(i), neurone.getError(), weightsDiff.get(i));

                double newWeight = weights.get(i) + weightDiff;
                weights.set(i, newWeight);
                weightsDiff.set(i, weightDiff);
            }
        }
    }

    /**
     * Возвращает выходные значения нейронов уровня
     *
     * @return Выходные значения нейронов уровня
     */
    public List<Double> getNeuronesOut() {
        List<Double> neuronesOut = new ArrayList<Double>(mNeuroneCount);

        for (Neurone neurone : mNeurones) {
            neuronesOut.add(neurone.getOut());
        }

        return neuronesOut;
    }

    /**
     * Вычисляет ошибку нейронов по алгоритму непоследнего уровня сети
     *
     * @param nextLayer Следующий уровень
     */
    public void calculateNeuronesErrorNotLastLayer(NeuralLayer nextLayer) {

        int curNeurone = 0;
        for (Neurone neurone : mNeurones) {
            double neuroneOut = neurone.getOut();

            //Вычисляем сумму взвешенных ошибок нейрона с дочерними нейронами

            double sumError = 0.0;//сумма взвешенных ошибок нейрона с дочерними нейронами

            for (Neurone childNeurone : nextLayer.getNeurones()) {
                sumError += childNeurone.getError() * childNeurone.getWeights().get(curNeurone);
            }

            neurone.setError(NeuroneOperations.errorNotLastLayerNeurone(neuroneOut, sumError));
            curNeurone++;
        }
    }

    /**
     * Вычисляет ошибку нейронов по алгоритму последнего уровня сети
     *
     * @param correctOutputs Правильные выходные значения сети
     */
    public void calculateNeuronesErrorLastLayer(List<Double> correctOutputs) {

        int curNeurone = 0;
        for (Neurone neurone : mNeurones) {
            double neuroneOut = neurone.getOut();
            double correctOut = correctOutputs.get(curNeurone);

            neurone.setError(NeuroneOperations.errorLastLayerNeurone(neuroneOut, correctOut));
            curNeurone++;
        }
    }

    @Override
    public String toString() {
        String res="нейронный уровень" +
                "\nЧисло нейронов " + mNeuroneCount +
                "\nНейроны";

        StringBuilder resBuilder=new StringBuilder(res);

        int i=1;
        for (Neurone neurone:mNeurones) {
            resBuilder.append('\n');
            resBuilder.append(i);
            resBuilder.append(' ');
            resBuilder.append(neurone.toString());
            i++;
        }

        return resBuilder.toString();
    }

    /**
     * Строковое представление уровня для записи в файл
     * 1: число_нейронов
     * 2: и т.д - нейроны уровня в формате:
     * 1: номер_нейрона число_весов
     * 2: и т.д: веса в формате: вес1 вес2...весn
     */
    public String toTextFile() {
        String result = "";

        StringBuilder sb = new StringBuilder(result);
        sb.append(mNeuroneCount);

        //Строковое представление нейронов уровня
        for (int i = 0; i < mNeuroneCount; i++) {
            sb.append("\r\n");
            sb.append(i);
            sb.append(' ');
            sb.append(mNeurones.get(i).toTextFile());
        }
        return sb.toString();
    }


}
