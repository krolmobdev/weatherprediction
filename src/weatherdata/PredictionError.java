package weatherdata;

/**
 * Описывает ошибки прогнозирования нейронной сетью и аппроксимацией
 */
public class PredictionError {
    private double mNeuralNetworkPredictionError;//ошибка прогнозирования нейронной сети
    private double mSplinePredictionError;//ошибка пронозирования аппроксимацией

    /**
     * Создание объекта со значениями по-умолчанию
     */
    public PredictionError() {
    }

    /**
     * Создание объета с заданными парметрами
     *
     * @param neuralNetworkPredictionError Ошибка прогнозирования нейронной сетью
     * @param splinePredictionError        Ошибка прогнозирования методом аппроксимации
     */
    public PredictionError(double neuralNetworkPredictionError, double splinePredictionError) {
        mNeuralNetworkPredictionError = neuralNetworkPredictionError;
        mSplinePredictionError = splinePredictionError;
    }

    /**
     * Вовращает ошибку прогнозирования нейронной сетью
     */
    public double getNeuralNetworkPredictionError() {
        return mNeuralNetworkPredictionError;
    }

    /**
     * Устанавливает ошибку пронозирования нейронной сетью
     *
     * @param neuralNetworkPredictionError Ошибка прогнозирования
     */
    public void setNeuralNetworkPredictionError(double neuralNetworkPredictionError) {
        mNeuralNetworkPredictionError = neuralNetworkPredictionError;
    }

    /**
     * Вовращает ошибку прогнозирования методом аппроксимации
     */
    public double getSplinePredictionError() {
        return mSplinePredictionError;
    }

    /**
     * Устанавливает ошибку прогнозирования методом аппроксимации
     *
     * @param splinePredictionError Ошибка прогнозирования методом аппроксимации
     */
    public void setSplinePredictionError(double splinePredictionError) {
        mSplinePredictionError = splinePredictionError;
    }

    @Override
    public String toString() {

        return "Ошибка прогноза нейросети:" + DataOperations.round(mNeuralNetworkPredictionError, 1) +
                "\nОшибка прогноза аппроксимацией:" + DataOperations.round(mSplinePredictionError, 1);
    }
}
