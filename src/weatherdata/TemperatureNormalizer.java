package weatherdata;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Нормализатор температуры
 */
public class TemperatureNormalizer implements Serializable {
    public static final double NORM_MIN_TEMPERATURE = 0;
    public static final double NORM_MAX_TEMPERATURE = 1;
    private static final long serialVersionUID = 1804696388343665783L;

    private double mMinTemperature;//Минимальное значение температуры
    private double mMaxTemperature;//Максимальное значение температуры

    /**
     * Создает нормализатор температуры
     *
     * @param minTemperature Минимальное значение температуры
     * @param maxTemperature Максимальное значение температуры
     */
    public TemperatureNormalizer(double minTemperature, double maxTemperature) {
        mMinTemperature = minTemperature;
        mMaxTemperature = maxTemperature;
    }

    /**
     * Сериализует объект нормализатора в файл
     *
     * @param serFilename Имя файла
     * @param normalizer  Объект нормализатора
     * @throws IOException При возникновении ошибки записи объекта в файл
     */
    public static void saveNormalizer(String serFilename, TemperatureNormalizer normalizer) throws IOException {
        ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(serFilename));
        os.writeObject(normalizer);
        os.close();
    }

    /**
     * Востанавливает сериализованный объект нормализатора из файла
     *
     * @param serFilename Имя файла
     * @throws IOException При возникновении ошибки чтения объекта из файла
     */
    public static TemperatureNormalizer loadNormalizer(String serFilename) throws IOException {
        try {
            if (!new File(serFilename).exists()) {
                throw new IOException("Отсутствует файл " + serFilename + " с сохраненным нормализатором");
            }

            ObjectInputStream os = new ObjectInputStream(new FileInputStream(serFilename));
            TemperatureNormalizer normalizer = (TemperatureNormalizer) os.readObject();
            os.close();

            return normalizer;
        } catch (ClassNotFoundException ex) {
            throw new IOException(ex.getMessage());
        }
    }

    /**
     * Вовращает максимальное значение температуры
     */
    public double getMaxTemperature() {
        return mMaxTemperature;
    }

    /**
     * Задает максимальное значение температуры
     */
    public void setMaxTemperature(double maxTemperature) {
        mMaxTemperature = maxTemperature;
    }

    /**
     * Вовзращает минимальное значение температуры
     */
    public double getMinTemperature() {
        return mMinTemperature;
    }

    /**
     * Задает минимальное значение температуры
     *
     * @param minTemperature Минимальное значение температуры
     */
    public void setMinTemperature(double minTemperature) {
        mMinTemperature = minTemperature;
    }

    /**
     * Получаем список нормированных значений температуры
     */
    public List<Double> getNormalizeTemperatures(List<Double> temperatures) {
        List<Double> normalizeTemperatures = new ArrayList<Double>();

        for (Double temperature : temperatures) {
            normalizeTemperatures.add(getNormedTemperature(temperature));
        }

        //Logger.getLogger(getClass().getCanonicalName()).log(Level.INFO,"Нормированный список температур:"+temperatures);

        return normalizeTemperatures;
    }

    /**
     * Возвращает нормализованное значение температуры
     *
     * @param temperature Исходное значение температуры
     */
    public double getNormedTemperature(double temperature) {
        return DataOperations.normalize(temperature, mMinTemperature, mMaxTemperature,
                NORM_MIN_TEMPERATURE, NORM_MAX_TEMPERATURE);
    }

    /**
     * Вовзращает ненормализованное значение температуры
     *
     * @param normedTemperature Нормализованное значение температуры
     */
    public double getNotNormedTemperature(double normedTemperature) {
        return DataOperations.normalize(normedTemperature, NORM_MIN_TEMPERATURE, NORM_MAX_TEMPERATURE,
                mMinTemperature, mMaxTemperature);
    }
}
