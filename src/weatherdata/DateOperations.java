package weatherdata;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Операции для работы с датой
 */
public final class DateOperations {
    private DateOperations(){
    }

    /**
     * Добавляет указанное количество дней к дате
     * @param date Дата
     * @param dayCount Колличество дней
     */
    public static Date addDaysToDate(Date date,int dayCount){
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        instance.add(Calendar.DAY_OF_MONTH, 1);

        return instance.getTime();
    }

    /**
     * Строковое представление даты
     * @param date Дата
     */
    public static String stringRepresentation(Date date){
        return new SimpleDateFormat("dd.MM.yyyy").format(date);
    }
}
