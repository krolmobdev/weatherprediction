package weatherdata;

public class WeatherDataRuntimeException extends RuntimeException {
    public WeatherDataRuntimeException() {
        super();
    }

    public WeatherDataRuntimeException(String message) {
        super(message);
    }
}
