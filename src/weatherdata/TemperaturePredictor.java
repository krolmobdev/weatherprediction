package weatherdata;

import neuralnetwork.NeuralNetwork;
import org.apache.commons.math3.stat.regression.SimpleRegression;

import java.util.ArrayList;
import java.util.List;

/**
 * Осуществляет прогнозирование температуры на следующий день и сравнительный анализ температуры на заданный период
 * методами нейронной сети и аппроксимации
 */
public class TemperaturePredictor {
    private TemperatureObservationsController mController;//Подготавливает даннные для прогноза
    private NeuralNetwork mNeuralNetwork;//Нейронная сеть для прогнозирования

    /**
     * Создает объект для пронозирования погоды
     *
     * @param controller Содержит данные наблюдений и методы для их подготовки для прогнозирования
     * @param network    Нейронная сеть, использующаяся для прогнозирования
     */
    public TemperaturePredictor(TemperatureObservationsController controller, NeuralNetwork network) {
        mController = controller;
        mNeuralNetwork = network;
    }

    /**
     * Возвращает объект для подготовки данных для прогноза
     */
    public TemperatureObservationsController getController() {
        return mController;
    }

    /**
     * Возвращает нейросеть, используемую для прогнозирования
     */
    public NeuralNetwork getNeuralNetwork() {
        return mNeuralNetwork;
    }


    /**
     * Возвращает температурый ряд из объектов
     * (дата, наблюдаемая температура, прогноз температуры нейросетью, прогноз температуры аппроксимацией,
     * ошибки пронозирования нейронной сетью и аппроксимацией)
     * Для предсказания используются ранее предсказанные значения температуры
     *
     * @param predictingValueCount Число дней, на которые выполняется прогноз
     */
    public List<ComparablePredictedTemperature> compareTemperatureSequenceUsingPredictedValues(int predictingValueCount) throws WeatherDataRuntimeException {
        //Данные для построения сравнительного ряда
        List<TemperatureObservation> sourceObservations = mController.getTemperatureObservations();

        //Сравнительный ряд
        List<ComparablePredictedTemperature> compareSequence = new ArrayList<ComparablePredictedTemperature>();

        // Подготавляиваем выборку. размером равным образу, распознавваемому нейронной сетью,
        // для прогноза первого значения сравнительного ряда
        List<TemperatureObservation> preparedObservationsForNeuralNetwork = new ArrayList<TemperatureObservation>();
        preparedObservationsForNeuralNetwork.addAll(sourceObservations.subList(0, mNeuralNetwork.getImageDimension()));

        List<TemperatureObservation> preparedObservationsForSpline = new ArrayList<TemperatureObservation>();
        preparedObservationsForSpline.addAll(preparedObservationsForNeuralNetwork);


        //Объекты для нормализации данных для получения значений сравнительного ряда
        TemperatureNormalizer temperatureNormalizer = mController.getTemperatureNormalizer();
        TemperatureObservationsController preparedObservationController
                = new TemperatureObservationsController(temperatureNormalizer);

        for (int i = 0; i < sourceObservations.size() - mNeuralNetwork.getImageDimension(); i++) {
            preparedObservationController.setTemperatureObservations(preparedObservationsForNeuralNetwork);
            List<Double> normalizeTemperaturesForNeuralNetwork = preparedObservationController.getNormalizeTemperatures();

            //Прогнозируем температуру на следующий день методами нейронной сети и аппроксимацией
            List<Double> neuralNetworkPredictions = mNeuralNetwork.recognize(normalizeTemperaturesForNeuralNetwork);

            preparedObservationController.setTemperatureObservations(preparedObservationsForSpline);
            List<Double> normalizeTemperaturesForSpline = preparedObservationController.getNormalizeTemperatures();

            SimpleRegression regression = new SimpleRegression();
            for (int j = 0; j < normalizeTemperaturesForSpline.size(); j++) {
                regression.addData(j, normalizeTemperaturesForSpline.get(j));
            }

            //Получает значение наблюдения соответствующего спронозированному значению температуры
            TemperatureObservation nextObservation = sourceObservations.get(i + mNeuralNetwork.getImageDimension());

            //Заполняет данные предсказанного наблюдения
            ComparablePredictedTemperature predictedTemperature = new ComparablePredictedTemperature();

            predictedTemperature.setNumber(i);
            predictedTemperature.setObservableTemperature(nextObservation.getTemperature());
            predictedTemperature.setObservationDate(nextObservation.getDate());

            double neuralNetworkPredictedTemperature = temperatureNormalizer.getNotNormedTemperature(neuralNetworkPredictions.get(0));
            predictedTemperature.setNeuralNetworkPredictedTemperature(neuralNetworkPredictedTemperature);

            double splinePredictedTemperature = temperatureNormalizer.getNotNormedTemperature(regression.predict(normalizeTemperaturesForSpline.size()));
            predictedTemperature.setSplinePredictedTemperature(splinePredictedTemperature);

            PredictionError predictionError = new PredictionError();

            predictionError.setNeuralNetworkPredictionError(Math.abs(nextObservation.getTemperature() - neuralNetworkPredictedTemperature));
            predictionError.setSplinePredictionError(Math.abs(nextObservation.getTemperature() - splinePredictedTemperature));

            predictedTemperature.setPredictionError(predictionError);

            //Добавляет предсказанное наблюдение в сравнительный ряд
            compareSequence.add(predictedTemperature);

            if ((i + 1) % predictingValueCount != 0) {
                //Если еще не предсказано predictingValueCount наблюдений,
                // то удалям первое значение выборки для получения прогноза и
                // добавляем предсказанное значение в конец выборки для получения прогноза
                // следующего значения сравнительного ряда

                preparedObservationsForNeuralNetwork.remove(0);
                preparedObservationsForNeuralNetwork.add(
                        new TemperatureObservation(predictedTemperature.getObservationDate(),
                                predictedTemperature.getNeuralNetworkPredictedTemperature()));

                preparedObservationsForSpline.remove(0);
                preparedObservationsForSpline.add(
                        new TemperatureObservation(predictedTemperature.getObservationDate(),
                                predictedTemperature.getSplinePredictedTemperature()));

            } else {
                //Если уже предсказано predictingValueCount наблюдений,
                // очищаем выборку для получения прогноза и формируем выборку из данных для прогноза,
                // равную образу, распознаваемому нейронной сетью,
                // для прогноза следующего значения сравнительного ряда

                preparedObservationsForNeuralNetwork.clear();
                preparedObservationsForNeuralNetwork.addAll(sourceObservations
                        .subList(i + 1, i + 1 + mNeuralNetwork.getImageDimension()));

                preparedObservationsForSpline.clear();
                preparedObservationsForSpline.addAll(preparedObservationsForNeuralNetwork);
            }
        }
        return compareSequence;
    }


    /**
     * Прогнозирует температуру нейронной сетью и аппроксимацией на заданное число дней
     *
     * @param dayCount Число дней, на которые прогнозируется тмпература
     */
    public List<PredictedTemperature> nextDayTemperature(int dayCount) throws WeatherDataException {
        if (dayCount == 0) {
            throw new WeatherDataException("Число дней для прогнозирования равно 0");
        }

        //Данные для построения сравнительного ряда
        List<TemperatureObservation> sourceObservations = mController.getTemperatureObservations();

        if (sourceObservations.size() == 0) {
            throw new WeatherDataException("Размер данных для прогноза равен нулю");
        }

        if (sourceObservations.size() != mNeuralNetwork.getImageDimension()) {
            throw new WeatherDataException("Размер выборки не совпадает с размерностью образа, распознаваемого нейросетью");
        }

        List<PredictedTemperature> predictedTemperatures = new ArrayList<PredictedTemperature>();

        // Подготавляиваем выборку. размером равным образу, распознавваемому нейронной сетью,
        // для прогноза первого значения сравнительного ряда
        List<TemperatureObservation> preparedObservationsForNeuralNetwork = new ArrayList<TemperatureObservation>();
        preparedObservationsForNeuralNetwork.addAll(sourceObservations);

        List<TemperatureObservation> preparedObservationsForSpline = new ArrayList<TemperatureObservation>();
        preparedObservationsForSpline.addAll(preparedObservationsForNeuralNetwork);

        //Объекты для нормализации данных для получения значений сравнительного ряда
        TemperatureNormalizer temperatureNormalizer = mController.getTemperatureNormalizer();
        TemperatureObservationsController preparedObservationController
                = new TemperatureObservationsController(temperatureNormalizer);

        preparedObservationController.setTemperatureObservations(preparedObservationsForSpline);
        List<Double> normalizeTemperaturesForSpline1 = preparedObservationController.getNormalizeTemperatures();

        SimpleRegression regression = new SimpleRegression();
        for (int j = 0; j < normalizeTemperaturesForSpline1.size(); j++) {
            regression.addData(j, normalizeTemperaturesForSpline1.get(j));
        }

        for (int i = 0; i < dayCount; i++) {
            preparedObservationController.setTemperatureObservations(preparedObservationsForNeuralNetwork);
            List<Double> normalizeTemperaturesForNeuralNetwork = preparedObservationController.getNormalizeTemperatures();

            //Прогнозируем температуру на следующий день методами нейронной сети и аппроксимацией
            List<Double> neuralNetworkPredictions = mNeuralNetwork.recognize(normalizeTemperaturesForNeuralNetwork);

            //Заполняет данные предсказанного наблюдения
            PredictedTemperature predictedTemperature = new PredictedTemperature();

            predictedTemperature.setObservationDate(mController.dateNextObservation());

            double neuralNetworkPredictedTemperature = temperatureNormalizer.getNotNormedTemperature(neuralNetworkPredictions.get(0));
            predictedTemperature.setNeuralNetworkPredictedTemperature(neuralNetworkPredictedTemperature);

            double splinePredictedTemperature = temperatureNormalizer.getNotNormedTemperature(regression.predict(i + normalizeTemperaturesForNeuralNetwork.size()));
            predictedTemperature.setSplinePredictedTemperature(splinePredictedTemperature);

            //Добавляет предсказанное наблюдение список прогнозируемых значений
            predictedTemperatures.add(predictedTemperature);

            //Удалям первое значение выборки для получения прогноза и
            // добавляем предсказанное значение в конец выборки для получения прогноза
            // следующего значения сравнительного ряда

            preparedObservationsForNeuralNetwork.remove(0);
            preparedObservationsForNeuralNetwork.add(
                    new TemperatureObservation(predictedTemperature.getObservationDate(), predictedTemperature.getNeuralNetworkPredictedTemperature()));
        }
        return predictedTemperatures;
    }


    /**
     * Вычисляет среднюю ошибку прогнозирования температуры нейронной сетью и аппроксимацией
     *
     * @param predictedTemperatures Спрогнозированные значения температуры
     */
    public static PredictionError averagePredictionError(List<ComparablePredictedTemperature> predictedTemperatures) {
        double averageNeuralNetworkError = 0.0;
        double averageSplinePredictionError = 0.0;

        for (ComparablePredictedTemperature prediction : predictedTemperatures) {
            PredictionError error = prediction.getPredictionError();

            averageNeuralNetworkError += error.getNeuralNetworkPredictionError();
            averageSplinePredictionError += error.getSplinePredictionError();
        }

        averageNeuralNetworkError /= predictedTemperatures.size();
        averageSplinePredictionError /= predictedTemperatures.size();

        return new PredictionError(averageNeuralNetworkError, averageSplinePredictionError);
    }

    /**
     * Вычисляет максимальную ошибку прогнозирования температуры нейронной сетью и аппроксимацией
     *
     * @param predictedTemperatures Спрогнозированные значения температуры
     */
    public static PredictionError maxPredictionError(List<ComparablePredictedTemperature> predictedTemperatures) {
        double maxNeuralNetworkError = 0.0;
        double maxSplineError = 0.0;

        for (ComparablePredictedTemperature prediction : predictedTemperatures) {
            PredictionError error = prediction.getPredictionError();

            double neuralNetworkError = error.getNeuralNetworkPredictionError();

            if (neuralNetworkError > maxNeuralNetworkError) {
                maxNeuralNetworkError = neuralNetworkError;
            }

            double splineError = error.getSplinePredictionError();

            if (splineError > maxSplineError) {
                maxSplineError = splineError;
            }
        }

        return new PredictionError(maxNeuralNetworkError, maxSplineError);
    }
}
