package weatherdata;

import java.util.Date;

/**
 * Описывает данные наблюдения за температурой воздуха
 */
public class TemperatureObservation {
    private Date mDate;//дата наблюдения
    private double mTemperature;//температура воздуха

    /**
     * Создает новое наблюдение за температурой со значениями по-умолчанию
     */
    public TemperatureObservation() {
        mDate = new Date();
    }

    /**
     * Создание нового наблюдения
     *
     * @param date        Дата наблюдения
     * @param temperature Значение температуры
     */
    public TemperatureObservation(Date date, double temperature) {
        mDate = date;
        mTemperature = temperature;
    }

    /**
     * Вовзращает дату наблюдения
     */
    public Date getDate() {
        return mDate;
    }

    /**
     * Вовзвращает строковое представление даты наблюдения
     */
    public String getStringFormatDate() {
        return DateOperations.stringRepresentation(mDate);
    }

    /**
     * Устанавливает дату наблюдения
     *
     * @param date Дата наблюдения
     */
    public void setDate(Date date) {
        this.mDate = date;
    }

    /**
     * Вовзращает температуру наблюдения
     */
    public double getTemperature() {
        return mTemperature;
    }

    /**
     * Устанавливает температуру наблюдения
     *
     * @param temperature Температура наблюдения
     */
    public void setTemperature(double temperature) {
        mTemperature = temperature;
    }

    @Override
    public String toString() {
        return "Дата:" + getStringFormatDate() +
                " Температура:" + Math.round(mTemperature);
    }
}
