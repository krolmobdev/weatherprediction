package weatherdata;

/**
 * Описывает данные пронозируемого наблюдения за температурой для сравнительного анализа.
 * Содержит сведения о наблюдаемой температуре, пронозируемой температуре методами нейронной сети
 * и аппроксимации и ошибках пронозирования
 */
public class ComparablePredictedTemperature extends PredictedTemperature {
    private int mNumber;//Номер наблюдения
    private double mObservableTemperature;//Наблюдаемая температура
    private PredictionError mPredictionError;//Ошибка прогнозирования

    /**
     * Создает объект по-умолчанию
     */
    public ComparablePredictedTemperature() {
        mPredictionError = new PredictionError();
    }

    /**
     * Возвращает номер наблюдения
     */
    public int getNumber() {
        return mNumber;
    }

    /**
     * Устанавливает номер наблюдения
     *
     * @param number Номер наблюдения
     */
    public void setNumber(int number) {
        mNumber = number;
    }

    /**
     * Вовращает наблюдаемую температуру
     */
    public double getObservableTemperature() {
        return mObservableTemperature;
    }

    /**
     * Устанавливает наблюдаемую температуру
     *
     * @param observableTemperature Наблюдаемая температура
     */
    public void setObservableTemperature(double observableTemperature) {
        mObservableTemperature = observableTemperature;
    }

    /**
     * Вовращает ошибку пронозирования
     */
    public PredictionError getPredictionError() {
        return mPredictionError;
    }

    /**
     * Устанавливает ошибку прогнозирования
     *
     * @param predictionError Ошибка прогнозирования
     */
    public void setPredictionError(PredictionError predictionError) {
        mPredictionError = predictionError;
    }

    @Override
    public String toString() {
        return "Номер:" + mNumber +
                "\nДата:" + DateOperations.stringRepresentation(getObservationDate()) +
                "\nНаблюдение:" + Math.round(mObservableTemperature) +
                "\nПрогноз нейросети:" + Math.round(getNeuralNetworkPredictedTemperature()) +
                "\nПрогноз аппроксимацией:" + Math.round(getSplinePredictedTemperature()) +
                "\n" + mPredictionError;
    }
}
