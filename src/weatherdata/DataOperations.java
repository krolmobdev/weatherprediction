package weatherdata;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Операции для работы с вещественными переменными
 */
public final class DataOperations {
    private DataOperations() {
    }

    /**
     * Нормирует значение переменной
     *
     * @param value      Переменная
     * @param minValue   Минимальное значение переменной
     * @param maxValue   Максимальное значение переменной
     * @param leftRange  Левая граница интервала
     * @param rightRange Правая граница интервала
     */
    public static double normalize(double value, double minValue, double maxValue, double leftRange, double rightRange) {
        return ((((value - minValue) * (rightRange - leftRange)) / (maxValue - minValue)) + leftRange);
    }

    /**
     * Округляет значение переменной до указанного числа знаков после запятой
     *
     * @param value Округляемое значение
     * @param scale Число знаков после запятой
     */
    public static double round(double value, int scale) {
        return new BigDecimal(value).setScale(scale, RoundingMode.HALF_UP).doubleValue();
    }
}
