package weatherdata;

public class WeatherDataException extends Exception {
    public WeatherDataException() {
        super();
    }

    public WeatherDataException(String message) {
        super(message);
    }
}
