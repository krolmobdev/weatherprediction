package weatherdata;

import java.util.Date;

/**
 * Описывает данные прогнозируемой температуры
 */
public class PredictedTemperature {
    private Date mObservationDate;//Дата наблюдения
    private double mNeuralNetworkPredictedTemperature;//Прогнозируемая нейронной сетью температура
    private double mSplinePredictedTemperature;//Прогнозируемая методом апроксимации температура

    /**
     * Создает новое пронозируемое наблюдение со значениями по-умолчанию
     */
    public PredictedTemperature() {
        mObservationDate = new Date();
    }

    /**
     * Возвращает дату наблюдения
     */
    public Date getObservationDate() {
        return mObservationDate;
    }

    /**
     * Устанавливает дату наблюдения
     *
     * @param observationDate Дата наблюдения
     */
    public void setObservationDate(Date observationDate) {
        mObservationDate = observationDate;
    }

    /**
     * Вовзращает температуру, спрогнозированную нейронной сетью
     */
    public double getNeuralNetworkPredictedTemperature() {
        return mNeuralNetworkPredictedTemperature;
    }

    /**
     * Устанавливает температуру, спрогнозированную нейронной сетью
     *
     * @param neuralNetworkPredictedTemperature Температура
     */
    public void setNeuralNetworkPredictedTemperature(double neuralNetworkPredictedTemperature) {
        mNeuralNetworkPredictedTemperature = neuralNetworkPredictedTemperature;
    }

    /**
     * Возвращает температуру, спрогнозированную методом аппроксимации
     */
    public Double getSplinePredictedTemperature() {
        return mSplinePredictedTemperature;
    }

    /**
     * Устанавливает температуру, спрогнозированную методом аппроксимации
     *
     * @param splinePredictedTemperature Температура
     */
    public void setSplinePredictedTemperature(double splinePredictedTemperature) {
        mSplinePredictedTemperature = splinePredictedTemperature;
    }

    @Override
    public String toString() {
        return "Дата:" + DateOperations.stringRepresentation(mObservationDate) +
                "\nПрогноз нейросети:" + DataOperations.round(mNeuralNetworkPredictedTemperature, 1) +
                "\nПрогноз аппроксимацией:" + DataOperations.round(mSplinePredictedTemperature, 1);
    }
}
