package weatherdata;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;

/**
 * Управляет данными наблюдений за температурой
 */
public class TemperatureObservationsController {
    public static final int SIZE_MIN_SAMPLE = 30;//Размер минимальной обучающей выборки

    private List<TemperatureObservation> mTemperatureObservations;//Наблюдения за температурой
    private TemperatureNormalizer mTemperatureNormalizer;//Нормализатор температуры

    /**
     * Создает объект для управления данными наблюдений за температурой с заданным нармализатором
     *
     * @param temperatureNormalizer Нормализатор температуры
     */
    public TemperatureObservationsController(TemperatureNormalizer temperatureNormalizer) {
        mTemperatureObservations = new ArrayList<TemperatureObservation>();
        mTemperatureNormalizer = temperatureNormalizer;
    }

    /**
     * Возвращает нормализатор температуры
     */
    public TemperatureNormalizer getTemperatureNormalizer() {
        return mTemperatureNormalizer;
    }

    /**
     * Устанавливает нормализатор температуры
     *
     * @param temperatureNormalizer Нормализатор температуры
     */
    public void setTemperatureNormalizer(TemperatureNormalizer temperatureNormalizer) {
        mTemperatureNormalizer = temperatureNormalizer;
    }

    /**
     * Получет список наблюдений
     */
    public List<TemperatureObservation> getTemperatureObservations() {
        return mTemperatureObservations;
    }

    /**
     * Задает список наблюдений
     *
     * @param temperatureObservations Наблюдения
     */
    public void setTemperatureObservations(List<TemperatureObservation> temperatureObservations) {
        mTemperatureObservations = temperatureObservations;
    }

    /**
     * Возвращает дату следующего наблюдения
     *
     * @throws WeatherDataRuntimeException Если размер температурных данных равен нулю
     */
    public Date dateNextObservation() {
        if (mTemperatureObservations.size() == 0) {
            throw new WeatherDataRuntimeException("Размер температурных данных равен нулю");
        }
        Date lastDate = mTemperatureObservations.get(mTemperatureObservations.size() - 1).getDate();
        Date nextDate = DateOperations.addDaysToDate(lastDate, 1);

        return nextDate;
    }

    /**
     * Загружает данные наблюдений из excel-документа
     *
     * @param filename Имя файла excel-документа
     * @throws IOException Исключение при загрузке данных
     */
    public void loadObservationsDataFromExcel(String filename) throws IOException {

        List<TemperatureObservation> temperatureObservationData = new ArrayList<TemperatureObservation>();

        HSSFWorkbook workbook = new HSSFWorkbook(new FileInputStream(filename));
        HSSFSheet sheet = workbook.getSheetAt(0);

        for (int r = 0; r <= sheet.getLastRowNum(); r++) {
            HSSFRow row = sheet.getRow(r);

            if(row==null || row.getCell(0)==null || row.getCell(1)==null){
                continue;
            }
            TemperatureObservation weatherValue = new TemperatureObservation();
            weatherValue.setDate(row.getCell(0).getDateCellValue());
            weatherValue.setTemperature(row.getCell(1).getNumericCellValue());

            temperatureObservationData.add(weatherValue);
        }
        workbook.close();

        mTemperatureObservations = temperatureObservationData;
    }

    /**
     * Проверяет допустим ли размер данных наблюдений для обучения сети
     */
    public boolean isAllowableObservationsSizeForLeaningSample() {
        return mTemperatureObservations.size() >= SIZE_MIN_SAMPLE;
    }

    /**
     * Подготавливает данные выборки для обучения нейронной сети
     *
     * @param imageDimension Размерность образа, подаваемого на вход нейронной сети
     * @throws WeatherDataRuntimeException Если размер обучающей выборки меньше 30
     */
    public Map<List<Double>, List<Double>> prepareLeaningSample(int imageDimension) throws WeatherDataRuntimeException {
        if (mTemperatureObservations.size() < 30) {
            throw new WeatherDataRuntimeException("Размер обучающей выборки меньше 30");
        }

        //Получаем список температур
        List<Double> temperatureData = mTemperatureNormalizer.getNormalizeTemperatures(getTemperatures());

        //Создаем копию списка температур
        List<Double> copyData = new ArrayList<Double>(temperatureData.size());
        copyData.addAll(temperatureData);

        List<List<Double>> samples = new ArrayList<List<Double>>();//Список обучающих выборок размера SAMPLE_LENGTH

        //Нарезаем обучающие выборки (временной ряд+следующее значение ряда) размером SAMPLE_LENGTH от 0,1...i-SAMPLE_LENGTH элемента
        for (int i = 0; copyData.size() - i > imageDimension + 1; i++) {
            samples.add(copyData.subList(i, i + imageDimension + 1));
        }

        //Logger.getLogger(getClass().getCanonicalName()).log(Level.INFO,"Выборки:"+samples);

        Map<List<Double>, List<Double>> leaningSample = new LinkedHashMap<List<Double>, List<Double>>();//обучающая выборка
        //Преобразуем каждую выборку элементов, число которых равно SAMPLE_LENGTH) в образ (последовательность из  элементов с номерами от 0 до SAMPLE_LENGTH-1)
        //и прогнозируемое значение под номером SAMPLE_LENGTH
        for (List<Double> sample : samples) {
            leaningSample.put(sample.subList(0, sample.size() - 1), sample.subList(sample.size() - 2, sample.size() - 1));
        }

        //Logger.getLogger(getClass().getCanonicalName()).log(Level.INFO,"Образы:"+leaningSample.entrySet());

        return leaningSample;
    }

    /**
     * Возвращает температуры наблюдений
     */
    public List<Double> getTemperatures() {
        List<Double> temperatures = new ArrayList<Double>();

        for (TemperatureObservation observation : mTemperatureObservations) {
            temperatures.add(observation.getTemperature());
        }

        return temperatures;
    }

    /**
     * Возвращает нормализованные температуры наблюдений
     */
    public List<Double> getNormalizeTemperatures() {
        return mTemperatureNormalizer.getNormalizeTemperatures(getTemperatures());
    }

    /**
     * Проверяет, допустим ли размер данных наблюдений для сравнительного анализа
     *
     * @param imageDimension Размерность образа, подаваемого на вход нейронной сети
     */
    public boolean isAllowableObservationsSizeForCompareSequence(int imageDimension) {
        return mTemperatureObservations.size() >= imageDimension + 1;
    }

    /**
     * Подготавливает данные наблюдений для сравнительного анализа,
     * который выполянет прогноз 1 следующего значения температуры
     *
     * @param imageDimension Размерность образа, подаваемого на вход нейронной сети
     */
    public List<List<TemperatureObservation>> prepareDataForCompareSequence(int imageDimension) throws WeatherDataRuntimeException {
        if (!isAllowableObservationsSizeForCompareSequence(imageDimension)) {
            throw new WeatherDataRuntimeException("Размер данных для сравнительного ряда меньше минимально допустимого значения");
        }

        List<List<TemperatureObservation>> data = new ArrayList<List<TemperatureObservation>>();

        //Создаем копию списка температур
        List<TemperatureObservation> copyData = new ArrayList<TemperatureObservation>(mTemperatureObservations.size());
        copyData.addAll(mTemperatureObservations);

        //Нарезаем обучающие выборки (временной ряд+следующее значение ряда) размером SAMPLE_LENGTH от 0,1...i-SAMPLE_LENGTH элемента
        for (int i = 0; mTemperatureObservations.size() - i > imageDimension; i++) {
            data.add(copyData.subList(i, i + imageDimension));
        }
        return data;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Температура воздуха\n");

        for (TemperatureObservation data : mTemperatureObservations) {
            sb.append(data.toString());
            sb.append('\n');
        }

        return sb.toString();
    }
}